<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'multisite');

/** MySQL database username */
define('DB_USER', 'multisite');

/** MySQL database password */
define('DB_PASSWORD', 'multisite');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

/* Cookies regarding login */
define('COOKIE_DOMAIN',false);
// define('COOKIE_DOMAIN', '.multisite.dev'); //replace with your domain name (the first blog)
define('COOKIEPATH', '/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|XW`CNGBk5g/hTqLA2m=$xU$5U>Eur<Qu1.Bx!C{tDaoyzA1bBtE?MNX[pR$/3k|');
define('SECURE_AUTH_KEY',  'g%/j<5(QV0~.1naEfw;U>L6OJI_r+gvY9767i/)rdDo:(2UL%m-xsd+<mJsBp~!^');
define('LOGGED_IN_KEY',    'ehR1*R<}Oh)c+roExl+nV/E!_b&F]hZg!%U|J.c(wcmWAb?6]azH@0z9Tjhm8(?/');
define('NONCE_KEY',        'z*[T)),SejFgNQEPG{Ldg3+qxjt=rYI&,*R*!*N;=;mJ1QeMzlm`k=cpJ1OV+Pv+');
define('AUTH_SALT',        'Bms:uzg~j}hvghhO`r>o4;7(_rWQ5_M_qrL%rdNH;hL`Vj/07Td|.-bCE*#-d/E]');
define('SECURE_AUTH_SALT', 'e18]|EoCcT8r|toU#JJ]98{[+BRF%H-^D)QoMv/!*|3e/%JVz]q3<t:)*WtQ{J^+');
define('LOGGED_IN_SALT',   '7mUJ,3^6/[P^UY(fL;d,p)glz9g.&|Y8I#gr2.uT[@Fz<2@1|K?C2FPQCO>sEsS)');
define('NONCE_SALT',       'jN9+<9+zs^V>=$A2~R{W|+9&PjBf|Ek5C,^;At;+-k- ~_J3ecQ,|Vqfjy&iseph');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);

$server = $_SERVER['REMOTE_ADDR'];
if($server == '127.0.0.1') { 
    define('DOMAIN_CURRENT_SITE', 'multisite.dev');
    // define('COOKIE_DOMAIN', '.multisite.dev'); //replace with your domain name (the first blog)
} else {
    define('DOMAIN_CURRENT_SITE', 'multisite.staging.click3x.com');
    // define('COOKIE_DOMAIN', '.multisite.staging.click3x.com'); //replace with your domain name (the first blog)
}

define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
