<?php 

/*

Template Name: R2B About

*/

get_header(); 

?>


<?php 
	// GET PROJECTS FOR THE FEATRUED PROJECTS SECTION OF HOME PAGE
	$team = array();

	$args = array(
        'post_type' => 'team',
        'posts_per_page' => -1
    );
    
    $the_query = new WP_Query( $args );
    
    if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
    	$id = get_the_ID();
    	$my_team = array();
        $my_team["title"] = get_the_title();
        $my_team["pic"] =  wp_get_attachment_url( get_post_thumbnail_id($id) );
        $my_team["permalink"] = get_the_permalink();
        $my_team["content"] = get_the_content();
        if(get_field('job_title', $post->ID)) {
        	$my_team["job_title"] = get_field('job_title', $post->ID);
        }


        array_push($team, $my_team);

    endwhile; 
    endif;

    wp_reset_postdata();

    // helper($team);
?>



<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php 
		$headlines = array();
		$headline_copy_modules = get_field('headline_copy_module');

		if($headline_copy_modules) {
			foreach ($headline_copy_modules as $key => $module) {
				array_push($headlines, $module);
			}
		}
	?>

<?php endwhile; endif; ?>
	<div id="page-container">
		<div id="page-content">

			<div id="container" class="clearfix r2b-about">


				<?php foreach ($headlines as $key => $head) : ?>

					<?php
						$class = 'outer';
						$style = '';
						if($key%2) {
							$class .= ' grey-bg about';
						} else {
							$class .= ' white-bg intro';
						}

						if($head['logos_image'] == 'image' && isset($head['image'])) {
							$style = ' style="background-image:url('.$head['image']['url'].'); background-size: '.$head['image']['width'].'px '.$head['image']['height'].'px;"';
						}
						// helper($head);
					?>

					<div class="<?php echo $class; ?>"<?php echo $style; ?>>
		                <div class="wrap clearfix">


		                	<?php if($key%2) { ?>
			                    <div class="half clearfix logo-wrapper">
			                		<?php if(isset($head['copy'])) { echo '<p>'.$head['copy'].'</p>'; } ?>
			                		<?php if(isset($head['logos'])) { 
			                			echo '<ul class="logos clearfix">';
			                			foreach ($head['logos'] as $key => $logo) {
			                				echo '<li>';
			                				// .helper($logo).
			                					echo '<div class="'.$logo['class'].'"><a href="'.$logo['link'].'" target="_blank">';
			                						if($logo['image_svg'] == 'svg') {
			                							echo $logo['svg_code'];
			                						} elseif($logo['image_svg'] == 'image') {
			                							echo '<img src="'.$logo['image'].'">';
			                						}
			                					echo '</a></div>';
			                				echo '</li>';
			                			}
			                			echo '</ul>';
			                		} ?>
			                	</div>
			                	<div class="half">
			                		<?php if(isset($head['headline'])) { 
			                        	$heading = str_replace('<p>', '<p class="headline gotham-book">', $head['headline']);
			                        	echo $heading;
			                        	// echo '<p class="headline gotham-book">'.$head['headline'].'</p>';
			                        } ?>
			                    </div>
			                <?php } else { ?>
			                	<div class="half clearfix">
			                        <?php if(isset($head['headline'])) { 
			                        	$heading = str_replace('<p>', '<p class="headline gotham-book">', $head['headline']);
			                        	echo $heading;
			                        	// echo '<p class="headline gotham-book">'.$head['headline'].'</p>';
			                        } ?>
			                    </div>
			                    <div class="half clearfix">
			                        <?php if(isset($head['copy'])) { echo '<p>'.$head['copy'].'</p>'; } ?>
			                    </div>
			                <?php } ?>


		                </div>
		            </div>

	        	<?php endforeach; ?>

	        	<div class="outer grey-bg header">
	                <div class="clearfix">
	                    <h2>The Team</h2>
	                </div>
	            </div>

	            <div class="outer team-gallery">
	                <div class="wrap clearfix">
	                    <ul class="team-wrap clearfix">
	                    	<?php foreach ($team as $key => $member) : ?>
		                        <li>
		                            <div class="team-inner">
		                                <div class="img-wrap"><img src="<?php echo $member['pic']; ?>"></div>
		                                <h3><?php echo $member['title']; ?></h3>
		                                <h4><?php echo $member['job_title']; ?></h4>
		                            </div>
		                        </li>
	                    	<?php endforeach; ?>
	                    </ul>
	                </div>
	            </div>


			</div>

		</div>
	</div>

<?php get_footer(); ?>