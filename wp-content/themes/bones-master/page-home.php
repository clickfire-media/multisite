<?php get_header(); ?>

			<div id="page-container">
        	 
        		<div id="page-content">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<header class="article-header">
							<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
						</header>

						<section class="entry-content cf" itemprop="articleBody">
							<?php
								// ECHO LINKS FOR ALL OF THE SITES
								$blogs = array(2,3,4);
								foreach ($blogs as $key => $blog) {
									$details = get_blog_details($blog);
									echo '<a href="'.$details->siteurl.'" target="_blank"><h1>'.$details->blogname.'</h1></a>';
								}
							?>
						</section>

					</article>

					<?php endwhile; endif; ?>

					<?php get_sidebar(); ?>

				</div>

			</div>

<?php get_footer(); ?>