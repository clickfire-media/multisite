<?php get_header(); ?>

			<div id="page-container">
        	 
        		<div id="project-page" class="page-content-inner">
					<div id="project-details-container">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							
							<?php
								$b_id = get_current_blog_id();

								// CFM
								if($b_id == 2) {									
									get_template_part( 'post-formats/format-cfm-header', get_post_format() );
									get_template_part( 'post-formats/format-cfm-project', get_post_format() );								
									get_template_part( 'post-formats/format-cfm-footer', get_post_format() );
								}
								// CLICK 3X
								if($b_id == 3) {
									get_template_part( 'post-formats/format-c3x-project', get_post_format() );
								}
								// REASON 2 BE
								if($b_id == 4) {
									get_template_part( 'post-formats/format-header', get_post_format() );
									get_template_part( 'post-formats/format-project', get_post_format() );
									get_template_part( 'post-formats/format-footer', get_post_format() );

								}
							?>

						<?php endwhile; ?>

						<?php endif; ?>

					</div>
				</div>

			</div>

<?php get_footer(); ?>
