<?php 
// cfm project
function buildBlock_cfm($project, $sendemail) {
    $img_parts = explode('/', $project['thumbnail']['sizes']['cfm-email']);
    array_pop( $img_parts );

    if($sendemail) {
        $path = 'img/';
    } else {
        $path = implode("/", $img_parts) ."/";
    }


    // helper($project['thumbnail']);

    $my_p = '<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" style="width: 800px; max-width: 800px; background-color:#f1f1f1;">
        <tbody>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="content">
                                    <table cellpadding="0" cellspacing="0" border="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td class="content_row" align="center" style="width: 800px;">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td class="one_half1" width="400">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="mobile_centered_table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <img src="'.$path. basename( $project['thumbnail']['sizes']['cfm-email'] ) .'" alt="'.$project['title'].'" width="100%" height="auto" border="0" style="display: block;" />
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>

                                                                <td class="nomobile" width="20"></td>

                                                                <td class="one_half1" width="400" valign="top">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="mobile_centered_table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="padding" height="20">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="dark_heading" align="center" style="color:#393939; font-weight: 700; font-size: 18px; font-family: \'Montserrat\', sans-serif; text-transform: uppercase; line-height: 18px; text-align: left;">'.$project['title'].'</td>
                                                                            </tr>';
                                                                            if($project['sub_title']) {
                                                                              $my_p .= '<tr>
                                                                                  <td height="15"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                  <td class="red_heading" align="center" style="color:#b4122d; font-weight: 400; font-size: 13px; font-family: \'Lato\', sans-serif; text-transform: none; line-height: 13px; text-align: left; font-style: italic; text-transform:uppercase;">'.$project['sub_title'].'</td>
                                                                                </tr>';
                                                                              }

                                                                    $my_p .= '<tr>
                                                                                <td height="15"></td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td class="dark_text" align="center" style="color:#686868; font-weight: 400; font-size: 13px; font-family: \'Lato\', sans-serif; text-transform: none; line-height: 25px; text-align: left;">'.$project['description'].'</td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td height="25"></td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td align="left">
                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="button" height="40" width="132" bgcolor="#b4122d" style="font-family: \'Montserrat\', sans-serif; font-size: 12px; color: #ffffff; text-align: center; font-weight: 700; text-transform: uppercase; border-radius: 20px;">
                                                                                                    <a class="button1_text" href="'.$project['link'].'" style="text-decoration: none; color: #ffffff;">view project</a>
                                                                                                </td>';
                                                                                            if($project['additional_links']) {
                                                                                              foreach ($project['additional_links'] as $key => $my_link) {
                                                                                                $my_p .= '<td width="20"></td>';
                                                                                                $my_p .= '<td class="button" height="40" width="132" bgcolor="#b4122d" style="font-family: \'Montserrat\', sans-serif; font-size: 12px; color: #ffffff; text-align: center; font-weight: 700; text-transform: uppercase; border-radius: 20px;">
                                                                                                      <a class="button1_text" href="'.$my_link['link_url'].'" style="text-decoration: none; color: #ffffff;">'.$my_link['link_text'].'</a>
                                                                                                    </td>';
                                                                                              }
                                                                                            }
                                                                                          $my_p .= '</tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="padding" height="20">&nbsp;</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="no-mobile-padding padding" style="width: 20px;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" width="800px" style="max-width: 800px;">
        <tbody>
            <tr>
                <td class="section_h" height="40"></td>
            </tr>
        </tbody>
    </table>';
    return $my_p;
}

// cfm email
function printHeader_cfm($title, $sendemail) {
    $title = $title;
    $sendemail = $sendemail;

    if($sendemail) {
        $path = 'img/';
    } else {
        $path = get_bloginfo('url').'/email-images/';
    }

  $head = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title>Click 3X</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="text/html;charset=utf-8" http-equiv="content-type" />

    <link href=\'http://fonts.googleapis.com/css?family=Lato:400,400italic,700|Montserrat:400,700\' rel=\'stylesheet\' type=\'text/css\' />

    <style type="text/css">
        @charset "utf-8";
        
        @media screen and (max-width: 768px) and (min-width: 381px) {
            body {
                margin: 0 auto !important;
                width: 600px !important;
            }
            .wrapper_table {
                width: 600px !important;
                margin: 0 auto;
                text-align: center;
            }
            .padding {
                width: 20px !important;
            }
            .content {
                width: 600px !important;
            }
            .content_row {
                width: 560px !important;
            }
            .one_third {
                width: 170px !important;
                margin-bottom: 30px;
            }
            .one_third1 {
                width: 167px !important;
                margin-bottom: 30px;
            }
            .one_half {
                width: 270px !important;
            }
            .one_half1 {
                width: 270px !important;
            }
            .one_half1 img {
                width: 270px !important;
                height: auto !important;
            }
            .one_half_full {
                width: 300px !important;
            }
            .one_half_full img {
                max-width: 300px !important;
                height: auto !important;
            }
            .one_half_full img.tabimg {
                max-width: 150px !important;
                height: auto !important;
            }
            .content_row img {
                max-width: 560px;
                height: auto !important;
            }
        }
        
        @media screen and (max-width: 381px) {

            .no-mobile-padding {
                width: 0px !important;
            }

            body {
                margin: 0 auto !important;
                width: 320px !important;
            }
            table[class="wrapper_table"] {
                width: 320px !important;
            }
            td[class="padding"] {
                width: 10px !important;
            }
            td[class="content"] {
                width: 320px !important;
            }
            td[class="content_row"] {
                width: 300px !important;
            }
            img[class="laptop"] {
                width: 300px !important;
                height: auto;
                margin: 0 auto !important;
            }
            img[class="client"] {
                width: 55px !important;
                height: auto;
                margin: 0 auto !important;
            }
            table[class="mobile_centered_table"] {
                margin: 0 auto !important;
                text-align: center !important;
            }
            td[class="one_third"] {
                display: block;
                width: 300px !important;
                margin-bottom: 30px;
                float: left;
                text-align: center !important;
            }
            td[class="one_third"] img {
                margin: 0 auto;
                max-width: 300px !important;
                height: auto !important;
            }
            td[class="one_third1"] {
                display: block;
                width: 300px !important;
                margin-bottom: 30px;
                float: left;
                text-align: center !important;
            }
            td[class="one_third1"] img {
                margin: 0 auto;
                max-width: 300px !important;
                height: auto !important;
            }
            td[class="one_third"] td[class="noresize"] {
                margin: 0 auto;
            }
            td[class="one_third_spacer"] {
                display: none;
            }
            td[class="one_half"] {
                width: 300px !important;
                display: block;
                margin-bottom: 20px;
                text-align: center;
            }
            td[class="one_half1"] {
                width: 300px !important;
                display: block;
                margin-bottom: 20px;
                text-align: center;
            }
            td[class="one_half_full"] {
                width: 320px !important;
                display: block;
            }
            td[class="one_half_full"] img {
                max-width: 320px !important;
                height: auto !important;
            }
            td[class="one_half_full"] img.tabimg {
                max-width: 160px !important;
                height: auto !important;
            }
            td[class="one_half"] img {
                margin: 0 auto;
                max-width: 300px !important;
                height: auto !important;
            }
            td[class="one_half1"] img {
                margin: 0 auto;
                max-width: 300px !important;
                height: auto !important;
            }
        }
        
        @media screen and (max-width: 381px) {
            td[class="nomobile"] {
                display: none !important;
            }
            td[class="section_h"] {
                height: 35px !important;
            }
            table[id="bg1"] {
                background-size: 220% 500px!important;
            }
            table[id="bg2"] {
                background-size: 300% 350px!important;
            }
            table[id="bg4"] {
                background-size: 200% 250px!important;
            }
            table[id="bg3"] {
                background-size: 150% 600px!important;
            }
            table[id="bg5"] {
                background-size: 300% 470px!important;
            }
            td[class="one_half_nomargins"] {
                width: 320px !important;
                float: left;
            }
            td[class="one_half_nomargins"] img {
                max-width: 320px !important;
                height: auto !important;
            }
        }
        
        @media screen and (max-width: 768px) and (min-width: 381px) {
            .tabtxt {
                font-size: 10px !important;
            }
            .tabtxt1 {
                font-size: 15px !important;
            }
            .buttontab {
                width: 100px !important;
            }
            .section_h {
                height: 50px !important;
            }
            #bg2 {
                background-size: 100% 300px !important;
            }
            #bg3 {
                background-size: 120% 350px !important;
            }
            #bg4 {
                background-size: 120% 350px !important;
            }
            #bg1 {
                background-size: 100% 380px !important;
            }
            .padding_phone {
                display: none;
            }
            .one_half_nomargins {
                width: 300px !important;
            }
            .one_half_nomargins img {
                max-width: 300px !important;
            }
            .one_third1 img {
                width: 170px !important;
                height: auto !important;
            }
        }

        a {
            color:inherit;
        }
    </style>

</head>

<body style="background-color: #ffffff; margin: 0 auto; width: 800px;">
    <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family:sans-serif;">
        '.$title.'
    </div>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" style="background-color: #ffffff; width: 800px; max-width: 800px;">
        <tbody>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="padding" style="width: 20px;">
                                </td>

                                <td class="content_row" align="center" style="width: 760px;">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td align="left">
                                                   <p>Hi [firstname,fallback],<br />Here is just a small selection of some of our latest projects this summer.<br />Enjoy! We\'d love to hear from you.</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td class="padding" style="width: 20px;">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" style="background-color: #ffffff; width: 800px; max-width: 800px;">
        <tbody>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="padding" style="width: 20px;">
                                </td>
                                <td class="content_row" align="center" style="width: 760px;">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td align="right" style="width:45%;">
                                                    <a href="http://click3x.com/">
                                                        <img align="center" src="'.$path.'click-logo.png" width="149" height="60" style="max-width:159px;" alt="click 3x logo"/>
                                                    </a>
                                                </td>
                                                <td align="left" style="width:55%;">
                                                    <a href="http://clickfiremedia.com/">
                                                        <img align="center" src="'.$path.'cfm-logo.png" width="260" height="60" style="max-width:260px;" alt="clickfire media logo"/>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td class="padding" style="width: 20px;">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <table id="bg5" align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" style="width: 800px; max-width: 800px; background: #b4122d url('.$path.'bg5.jpg) 50% 0% / cover no-repeat">
        <tbody>
            <tr>
                <td>
                    <!--[if gte mso 9]>
  <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="background-color:#f1f1f1;">
    <v:fill type="tile" src="'.$path.'bg5.jpg" color="#f9f9f9" />
    <v:textbox inset="0,0,0,0">
  <![endif]-->
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="content">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="padding" style="width: 20px;">
                                                </td>
                                                <td class="content_row" align="center" style="width: 760px;">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td class="section_h" height="105"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="white_heading" align="center" style="color:#FFFFFF; font-weight: 700; font-size: 30px; font-family: \'Montserrat\', sans-serif; text-transform: uppercase; line-height: 1.4em;">'.$title.'</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="section_h" height="102"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="padding" style="width: 20px;">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!--[if gte mso 9]>
    </v:textbox>
  </v:rect>
  <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" width="800px" style="max-width: 800px;">
        <tbody>
            <tr>
                <td class="section_h" height="40"></td>
            </tr>
        </tbody>
    </table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" style="width: 800px; max-width: 800px;">
        <tbody>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0" style="margin:auto;">
                        <tbody>
                            <tr>
                                <td class="content">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="padding" style="width: 20px;">
                                                </td>
                                                <td class="content_row" align="center" style="width: 760px;">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td class="dark_heading" align="center" style="color:#505050; font-weight: 700; font-size: 22px; font-family: \'Montserrat\', sans-serif; text-transform: uppercase; line-height: 31px; text-align:center; max-width: 25em;"> - Latest Projects - </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="padding" style="width: 20px;">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" width="800px" style="max-width: 800px;">
        <tbody>
            <tr>
                <td class="section_h" height="40"></td>
            </tr>
        </tbody>
    </table>';

    return $head;
}


function cfmFooterContent($footer_content) {
    $foot ='<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" style="background-color: #ffffff; width: 800px; max-width: 800px;">
          <tbody>
              <tr>
                  <td>
                      <table cellpadding="0" cellspacing="0" border="0">
                          <tbody>
                              <tr>
                                  <td class="content">
                                      <table cellpadding="0" cellspacing="0" border="0">
                                          <tbody>
                                              <tr>
                                                  <td class="padding" style="width: 5px;"></td>
                                                  <td class="content_row" align="center" style="width: 790px;">
                                                      <table cellpadding="0" cellspacing="0" border="0">
                                                          <tbody>';
                                                        if($footer_content) {
                                                            $foot .='<tr><td>'.$footer_content.'</td></tr>';
                                                        }
                                                        $foot .='</tbody>
                                                      </table>
                                                  </td>
                                                  <td class="padding" style="width: 5px;"></td>
                                              </tr>
                                          </tbody>
                                      </table>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                  </td>
              </tr>
          </tbody>
      </table>';

      return $foot;
}



function printFooter_cfm($project, $footer_content, $sendemail) {
    $project = $project;
    $footer_content = $footer_content;
    $sendemail = $sendemail;

    if($sendemail) {
        $path = 'img/';
    } else {
        $path = get_bloginfo('url').'/email-images/';
    }

  $foot = '<table id="bg5-2" align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" style="width: 800px; max-width: 800px; background: #b4122d url('.$path.'E-mail_Footer.jpg) 50% 0% / cover no-repeat">
          <tbody>
              <tr>
                  <td>
                      <!--[if gte mso 9]>
    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="background-color:#f1f1f1;">
      <v:fill type="tile" src="'.$path.'E-mail_Footer.jpg" color="#f1f1f1" />
      <v:textbox inset="0,0,0,0">
    <![endif]-->
                      <table cellpadding="0" cellspacing="0" border="0">
                          <tbody>
                              <tr>
                                  <td class="content">
                                      <table cellpadding="0" cellspacing="0" border="0">
                                          <tbody>
                                              <tr>
                                                  <td class="padding" style="width: 20px;">
                                                  </td>
                                                  <td class="content_row" align="center" style="width: 760px;">
                                                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                          <tbody>
                                                              <tr>
                                                                  <td class="section_h" height="65"></td>
                                                              </tr>
                                                              <tr>
                                                                  <td class="white_heading" align="center" style="color:#FFF; font-weight: 700; font-size: 30px; font-family: \'Montserrat\', sans-serif; text-transform: uppercase; line-height: 1.4em;">want to see more?</td>
                                                              </tr>
                                                              <tr>
                                                                  <td height="10"></td>
                                                              </tr>
                                                              <tr>
                                                                  <td align="center">
                                                                      <table cellpadding="0" cellspacing="0" border="0">
                                                                          <tbody>
                                                                              <tr>
                                                                                  <td class="button" height="40" width="158" bgcolor="#b4122d" style="letter-spacing:0.1em; font-family: \'Montserrat\', sans-serif; font-size: 12px; color: #ffffff; text-align: center; font-weight: 400; text-transform: uppercase; border-radius: 20px; padding-left:3em; padding-right:3em;">
                                                                                      <a class="button1_text" href="http://click3x.com/" style="text-decoration: none; color: #ffffff;">check out our site</a>
                                                                                  </td>
                                                                              </tr>
                                                                          </tbody>
                                                                      </table>
                                                                  </td>
                                                              </tr>
                                                              <tr>
                                                                  <td class="section_h" height="70"></td>
                                                              </tr>
                                                          </tbody>
                                                      </table>
                                                  </td>
                                                  <td class="padding" style="width: 20px;">
                                                  </td>
                                              </tr>
                                          </tbody>
                                      </table>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                      <!--[if gte mso 9]>
      </v:textbox>
    </v:rect>
    <![endif]-->
                  </td>
              </tr>
          </tbody>
      </table>
      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" style="width: 800px; max-width: 800px; background-color: rgb(249, 249, 249);">
          <tbody>
              <tr>
                  <td>
                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tbody>
                              <tr>
                                  <td class="content">
                                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                          <tbody>
                                              <tr>
                                                  <td height="45" width="33.33334%" align="center" class="footer" bgcolor="#ec435c">
                                                      <a href="https://twitter.com/Click3X/" target="_blank">
                                                          <img src="'.$path.'tw.png" width="12" height="10" border="0" style="display: block;" alt="Twitter" />
                                                      </a>
                                                  </td>
                                                  <td height="45" width="33.33334%" align="center" class="bg_primary" bgcolor="#b4122d">
                                                      <a href="https://www.facebook.com/Click3X/" target="_blank">
                                                          <img src="'.$path.'fb.png" width="7" height="13" border="0" style="display: block;" alt="Facebook" />
                                                  </a>
                                                  </td>
                                                  <td height="45" width="33.33334%" align="center" class="footer" bgcolor="#ec435c">
                                                      <a href="https://www.linkedin.com/company/click-3x" target="_blank">
                                                          <img src="'.$path.'in.png" width="13" height="12" border="0" style="display: block;" alt="Linked In" />
                                                      </a>
                                                  </td>
                                              </tr>
                                          </tbody>
                                      </table>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                  </td>
              </tr>
          </tbody>
      </table>
      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" width="800px" style="background-color: #ffffff; max-width: 800px;">
          <tbody>
              <tr>
                  <td class="section_h" height="60"></td>
              </tr>
          </tbody>
      </table>';
  
    $foot .= cfmFooterContent($footer_content);
  
      $foot .= '<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" width="800px" style="background-color: #ffffff; width:100%; max-width: 800px;">
          <tbody>
              <tr>
                  <td class="section_h" height="60"></td>
              </tr>
          </tbody>
      </table>
  </body>
  </html>';

  return $foot;
}

?>