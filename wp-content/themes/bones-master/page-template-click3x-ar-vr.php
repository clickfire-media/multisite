<?php 

/*

Template Name: Click3X-AR-VR

*/

get_header(); 

	// GET PROJECTS FOR THE FEATRUED PROJECTS SECTION OF HOME PAGE
	$projects = array();

	$args = array(
        'post_type' => 'project',
        'posts_per_page' => -1,
        'tax_query' => array(                     //(array) - use taxonomy parameters (available with Version 3.1).
            'relation' => 'AND',                      //(string) - Possible values are 'AND' or 'OR' and is the equivalent of ruuning a JOIN for each taxonomy
            array(
                'taxonomy' => 'project_category',                //(string) - Taxonomy.
                'field' => 'slug',                    //(string) - Select taxonomy term by ('id' or 'slug')
                'terms' => array( 'c3xvr' ),    //(int/string/array) - Taxonomy term(s).
                'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
                'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
            )
        )
    );
    
    $the_query = new WP_Query( $args );
    
    if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();

    	$my_project = array();
        $my_project["title"] = get_the_title();
        if(get_field('project_link')) {
        	$my_project["permalink"] = get_field('project_link');
        } else {
        	$my_project["permalink"] = get_the_permalink();
        }
        if(get_field('client')) {
        	$my_project["client"] = get_field('client');
        }
        if(get_field('poster')) {
        	$my_project["poster"] = get_field('poster');
        }
        if(get_field('video')) {
        	$my_project["video"] = get_field('video');
        }
        if(get_field('description')) {
        	$my_project["description"] = get_field('description');
        }

        array_push($projects, $my_project);

    endwhile; 
    endif;

    wp_reset_postdata();

?>

			<div id="page-container">
        	 
        		<div id="page-content">
					<div id="director-page" class="page-content-inner">
						<div id="director-details-container">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<?php 
									echo '<div class="page-header"><div class="page-header-inner">';
										if(get_field('headline')) {
											echo '<h1>'.get_field('headline').'</h1>';
										} else {
											echo '<h1>'.get_the_title().'</h1>';
										}

										the_content();
									echo '</div></div>';
								?>

								<?php if($projects) : ?>
                                    <div class="director-videos-list">
                                        <ul>
                                            <?php foreach ($projects as $key => $project) : ?>
                                            	<?php 
                                                    // helper($project);
                                                    $title = $project['title'];
                                                    if($project['poster']) {
                                                        $poster = $project['poster'];
                                                        $url = $poster['url'];
                                                    } else {
                                                        $url = '';
                                                    }
                                                    if($project['video']) {
                                                        $video = $project['video'];
                                                        // helper($video);
                                                        $vid_filename = $video['title'];
                                                        $vid_url = $video['url'];
                                                    } else {
                                                        $vid_url = '';
                                                    }

                                                ?>
                                                <li data-id="<?php echo $d_id; ?>" class="director-item hoverfriend" data-filename="<?php echo $vid_url; ?>">
                                                    <a href="<?php echo $project['permalink']; ?>" style="background-image: url(<?php echo $url;?>);"><div class="red-gradient"></div></a>
                                                    <div class="video-label">
                                                        <?php if(isset($project['client'])) {
                                                            echo '<h3>'.$project['client'].'</h3>';
                                                         } ?>
                                                        <h2><?php echo $title; ?></h2>
                                                        <div class="label-line"></div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>


							<?php endwhile; ?>

							<?php endif; ?>

						</div>
					</div>
				</div>

			</div>

<?php get_footer(); ?>