<?php 
/*

Template Name: Click3x-Home

*/

get_header(); 

?>

		<div id="page-container">
        	 
        	<div id="page-content">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php 
						$page_links = get_field('page_links');
						$p_projects = array();

						foreach ($page_links as $key => $page_link) {
							$m_project = array();
							$m_project['id'] = $page_link['page_link']->ID;
							$m_project['title'] = $page_link['page_link']->post_title;
							$m_project['permalink'] = $page_link['page_link']->guid;

							if(get_field('featured_post', $m_project['id'])) {
								$feat_post = get_field('featured_post', $m_project['id']);
								$f_id = $feat_post->ID;

								$m_project['project_title'] = $feat_post->post_title;
								if(get_field('client', $f_id)) {
							    	$m_project["client"] = get_field('client', $f_id);
							    } 

							    if(has_post_thumbnail($page_link['page_link']->ID)) {
							    	$m_project["poster"]["url"] =  wp_get_attachment_url( get_post_thumbnail_id($page_link['page_link']->ID) );
							    } else if(get_field('poster', $f_id)) {
							    	$m_project["poster"] = get_field('poster', $f_id);
							    }

							    if(get_field('video', $f_id)) {
							    	$m_project["video"] = get_field('video', $f_id);
							    }
							    if(get_field('description', $f_id)) {
							    	$m_project["description"] = get_field('description', $f_id);
							    }
							} 
							// helper($m_project);
							array_push($p_projects, $m_project);
						}
					?>
					
					<div id="home-page" class="page-content-inner">

						<div class="featured-carousel-container">
							<div class="home-swiper-container swiper-container-horizontal">
							    <!-- Additional required wrapper -->
							    <div class="swiper-wrapper">
							    	<!-- Slides -->
							    	<?php foreach ($p_projects as $key => $project) : ?>
							    		<!-- <div class="swiper-slide" data-swiper-slide-index="<?php echo $key; ?>"> -->
							    		<div class="swiper-slide">
								      		<a data-id="<?php echo $project['id']; ?>" class="featured-item" data-filename="<?php echo $project['video']['title']; ?>"><div class="featured-poster" style="background-image: url(<?php echo $project['poster']['url']; ?>);"><div class="gray-filter"></div></div></a>
								        	<div class="video-credits">
								        		<h2><?php echo $project['client']; ?></h2>
								        		<a href="<?php echo $project['permalink']; ?>" data-filename="c3xvr" class="featured-item"><h1><?php echo $project['title']; ?></h1></a>
									        	<div class="label-line"></div>
									        </div>
									   	</div>
								    			
							    	<?php endforeach; ?>
							    </div>
							    <!-- fancy animatiion *_* -->
							    <div id="animation-over-slides">             										
									<div id="shape-container" style="display: none; left: 100%;">
					    				<span class="Shape_mid"></span>
					    				<span class="Shape_base"></span>
					    			</div>	
					                <div id="shape-containerA" style="left: 100%; display: none;">
					    				<span class="ShapeA_mid"></span>
					    				<span class="ShapeA_base"></span>
					    			</div>	
										<div id="shape-container2" style="display: none; left: -100%;">
					    				<span class="Shape2_mid"></span>
					    				<span class="Shape2_base"></span>
					    			</div>	
					                <div id="shape-container2A" style="left: -100%; display: none;">
					    				<span class="Shape2A_mid"></span>
					    				<span class="Shape2A_base"></span>
					    			</div>	
								</div>
							    
							    <div class="swiper-pagination swiper-pagination-clickable"><span class="swiper-pagination-bullet"><span class="swiper-bullet-inner"></span></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active"><span class="swiper-bullet-inner"></span></span><span class="swiper-pagination-bullet"><span class="swiper-bullet-inner"></span></span></div>

					            <div id="company-container" class="company-container clearfix">
					            	<?php foreach ($p_projects as $key => $project) : ?>
					            		<!-- account for the active index -->
					            		<?php
					            			if($key == 0) {
					            				echo '<div class="active-index"><a href="'.$project['permalink'].'">'.$project['title'].'</a></div>';
					            			} else {
					            				echo '<div class=""><a href="'.$project['permalink'].'">'.$project['title'].'</a></div>';
					            			}
					            		?>
					            	<?php endforeach; ?>
					            </div>

							</div>
						</div>	

					</div>
				
				<?php endwhile; endif; ?>

			</div>

		</div>

<?php get_footer(); ?>