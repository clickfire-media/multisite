<?php 

/*

Template Name: Click3X-Work

*/

get_header(); 

?>

		<div id="page-container">

			<div id="page-content">
        	 
	        	<div id="work-page" class="page-content-inner intro">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<div class="title-label"><h3><?php echo get_the_title(); ?></h3></div>

						<?php

							$featured_projects = get_field('featured_projects');

							if($featured_projects) : ?>

								<div class="cfm-project-gallery">
									<ul>
										<?php foreach ($featured_projects as $key => $project) : ?>
											<?php 
												$terms = get_the_terms($project->ID, 'agency');
												if($terms) {
													$term = $terms[0]->name;
												}
												$poster = get_field('poster', $project->ID);

												if(get_field('project_link', $project->ID)) {
													$link = get_field('project_link', $project->ID);
												} else {
													$link = $project->guid;	
												}
											?>
											<li class="work-item">
												<div class="project-inner hoverfriend">
													<a href="<?php echo $link; ?>" class="cfm-project bg_image_view ready" style="background-image: url(<?php echo $poster['url']; ?>);">
														<div class="gray-filter"></div>
														<div class="red-gradient"></div>
													</a>
													<div class="project-label"><div class="project-label-inner">
														<?php if(get_field('client', $project->ID)) : ?><h3><?php echo get_field('client', $project->ID); ?></h3><?php endif; ?>
														<?php  // if(isset($term)) { echo '<h3>'.$term.'<?h3>'; } ?>
														<h2><?php echo $project->post_name; ?></h2>
														<div class="label-line"></div>
														
													</div></div>
												</div>
											</li>

										<?php endforeach; ?>
									</ul>
								</div>

						<?php endif; ?>

					<?php endwhile; endif; ?>

					
				</div>

			</div>
			
		</div>
<?php get_footer(); ?>