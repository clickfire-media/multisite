<?php $b_id = get_current_blog_id(); ?>
<?php $current_blog = whichBlog(); ?>
<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js <?php echo $current_blog; ?>"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

        <!-- FAVICONS -->
        <?php include('php/favicon.php'); ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <script>var base_url = '<?php echo get_bloginfo("url"); ?>';</script>
    
		<?php 
            // IS EMAIL PAGE, LOAD JOSEFIN SANS FONT
            if(is_singular('email')) {
                echo '<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700" rel="stylesheet">';
            }

            // LOAD GOOGLE MAPS FOR CLICK3X AND R2B CONTACT PAGES
            if(is_page(47) || is_page(14)) {
                echo '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaTC6YnzPequMZihfh2Cbdr6CjcrhvE_k"></script>';
            }

            // IF CLICKFIRE MEDIA, LOAD CALLUA FONT
            if($b_id == 2) {
                echo '<script src="https://use.typekit.net/hqh3atb.js"></script>';
                echo '<script>try{Typekit.load({ async: true });}catch(e){}</script>';
            }
        ?>

		<?php wp_head(); ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
        
	</head>

	<body <?php body_class($current_blog); ?> itemscope itemtype="http://schema.org/WebPage">

		<div id="site-wrapper" class="site-wrapper">
            
            <div id="header-container">
                
                <div class="header-inner">
                    <!-- HEADER LOGO -->
                    <?php 
                        if($b_id == 3) {
                            echo '<div class="logo-may">
                                    <a href="http://click3x.com/intro" class="click-3x-logo">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 398 80" enable-background="new 0 0 398 80" xml:space="preserve">
                                            <g>
                                                <path d="M66.4,64.8c-6.8,9.4-16.2,13.2-26.8,13.2C17.5,77.9,2.2,62,2.2,40S17.5,2.1,39.6,2.1c9.8,0,18.6,3.3,24.9,11.3L59,18
                                                    c-4.5-6.7-12.1-9.8-19.4-9.8C20.7,8.2,9,22,9,40s11.7,31.8,30.6,31.8c7.2,0,16.2-3.5,21.3-11.2L66.4,64.8z"></path>
                                                <path d="M78.7,3.9h6.7V70h33.7v6.1H78.7V3.9z"></path>
                                                <path d="M129.7,3.9h6.7v72.2h-6.7V3.9z"></path>
                                                <path d="M215.7,64.8c-6.8,9.4-16.2,13.2-26.8,13.2c-22,0-37.3-15.9-37.3-37.9s15.3-37.9,37.3-37.9c9.8,0,18.6,3.3,24.9,11.3
                                                    l-5.5,4.6c-4.5-6.7-12.1-9.8-19.4-9.8C170,8.2,158.3,22,158.3,40s11.7,31.8,30.6,31.8c7.2,0,16.2-3.5,21.3-11.2L215.7,64.8z"></path>
                                                <path d="M228.1,3.9h6.7v31.8h0.6l35-31.8h9.3l-36.5,33l38.6,39.2H272l-36.6-37.3h-0.6v37.3h-6.7V3.9z"></path>
                                                <path fill="#b4132d" id="red-x" d="M291.3,61c2.7,6.9,7,10.8,15.2,10.8c10.7,0,16.1-7,16.1-15.7c0-10.7-9.9-14.9-19.1-14.9h-2.7v-6.1h3.1
                                                    c7.8,0,16.1-3.8,16.1-13.7c0-7.7-5.3-13.3-13.4-13.3c-5.9,0-10.9,3.3-13.7,8.5l-5.8-3.3c4-7.4,11.2-10.7,19.5-10.7
                                                    c11,0,20.1,6.1,20.1,18.6c0,7.9-4.5,14.5-12.9,16.7v0.2c9.2,1.6,15.4,9.3,15.4,18.5c0,11.4-7.4,20.7-22.3,20.7
                                                    c-10.8,0-18.6-4.4-22.3-14.2L291.3,61z"></path>
                                                <path d="M360.8,38.2L336.1,3.9h8l20.8,29.4l20.5-29.4h8.1l-24.5,34.3l26.8,37.9h-8.4l-22.8-32.6l-22.8,32.6h-8.1L360.8,38.2z"></path>
                                            </g>
                                        </svg>
                                    </a>
                                    <div class="label-line"></div>
                                </div>';
                            } else {
                            if(get_field('header_logo', 'option')) { 
                                $header_logo = get_field('header_logo', 'option');
                                echo '<h1 class="hide-h1">'.get_bloginfo('name').'</h1>';
                                echo '<div id="logo" class="logo" style="width:'.$header_logo['width'].'px; height:'.$header_logo['height'].'px;"><a href="'.home_url().'" style="width:'.$header_logo['width'].'px; height:'.$header_logo['height'].'px; display:block; background-image:url('.$header_logo['url'].');"></a></div>';

                                
                                if($b_id == 3) {
                                    echo '<div class="label-line"></div>';
                                }
                            }
                        }
                    ?>

                    <!-- MOBILE BUTTON -->
                    <div id="hamburger" class="hamburger">
                        <div class="hamburger-inner">
                            <a>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                    </div>

                    <!-- MAIN MENU -->
                    <?php if($b_id != 3) : ?>
    					<div id="page-labels">
    						<?php wp_nav_menu(array(
        					         'container' => false,                           // remove nav container
        					         'container_class' => 'menu cf',                 // class of container (should you choose to use it)
        					         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
        					         'menu_class' => 'nav top-nav cf',               // adding custom nav class
        					         'theme_location' => 'main-nav',                 // where it's located in the theme
        					         'before' => '',                                 // before the menu
            			               'after' => '',                                  // after the menu
            			               'link_before' => '',                            // before each link
            			               'link_after' => '',                             // after each link
            			               'depth' => 0,                                   // limit the depth of the nav
        					         'fallback_cb' => ''                             // fallback function (if there is one)
    						)); ?>
    					</div>
                    <?php endif; ?>

				</div>

			</div>
