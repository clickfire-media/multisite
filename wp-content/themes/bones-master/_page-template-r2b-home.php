<?php 

/*
	Template Name: R2B Home
*/

get_header(); 

?>


<?php 
	// GET PROJECTS FOR THE FEATRUED PROJECTS SECTION OF HOME PAGE
	$projects = array();

	$args = array(
        'post_type' => 'project',
        'posts_per_page' => -1,
        'tax_query' => array(                     //(array) - use taxonomy parameters (available with Version 3.1).
            'relation' => 'AND',                      //(string) - Possible values are 'AND' or 'OR' and is the equivalent of ruuning a JOIN for each taxonomy
            array(
                'taxonomy' => 'project_category',                //(string) - Taxonomy.
                'field' => 'slug',                    //(string) - Select taxonomy term by ('id' or 'slug')
                'terms' => array( 'featured' ),    //(int/string/array) - Taxonomy term(s).
                'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
                'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
            )
        )
    );
    
    $the_query = new WP_Query( $args );
    
    if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();

    	$my_project = array();
        $my_project["title"] = get_the_title();
        $my_project["gallery_thumbnail"] = get_field('gallery_thumbnail');
        $my_project["permalink"] = get_the_permalink();


        array_push($projects, $my_project);

    endwhile; 
    endif;

    wp_reset_postdata();
?>

		<div id="page-container">
    	 
    		<div id="home-page" class="page-content-inner">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php 
						$marquee_video = get_field('marquee_video');
						$marquee_poster = get_field('marquee_poster');
						$featured_case_studies = get_field('featured_case_studies');
						$clients = get_field('clients');
						$reel_video = get_field('reel_video');
						$reel_poster = get_field('reel_poster');

						// helper($marquee_video);
					?>

					<!-- MARQUEE VIDEO -->
					<?php if($marquee_video) : ?>
						<div id="reel-video" class="cfm-videoplayer autoplay paused" data-video-name="<?php echo $marquee_video['url']; ?>" data-poster="<?php echo $marquee_poster['url']; ?>">
						    <div class="cfm-videoplayer-inner">
						    	<div class="video-anchor-link"></div>
					            
					            <video class="cfm-videoplayer-mobile" width="960" height="540" controls src="<?php echo $marquee_video['url']; ?>" style="height: 100%; width: auto;"></video>

					            <div class="cfm-videoplayer-poster bg_image_view" style="background-image: url(<?php echo $marquee_poster['url']; ?>);">
					            	<div class="cfm-videoplayer-poster-inner">
										 <div class="cfm-videoplayer-playbutton"><span class="arrow"></span></div>
									</div>
					            </div>

					            <video class="cfm-videoplayer-desktop" width="960" height="540" loop src="<?php echo $marquee_video['url']; ?>" style="height: 100%; width: auto;"></video>
					            
					            <div class="cfm-video-controls" style="opacity: 0.75;">
					            	<ul>
										<li class="cfm-video-play-pause-btn cfm-video-btn"></li>
										<li class="cfm-video-fullscreen-btn cfm-video-btn"></li>
										<li class="cfm-video-mute-btn cfm-video-btn"></li>
									</ul>
									<div class="cfm-video-progress-container" style="background-size: 100% 40px;">
										<a class="cfm-video-seek-bar" style="width: 45.8613%;"></a>
										<input class="cfm-video-seek-bar-input" type="range" value="0">
									</div>
								</div>

						    </div>
						</div>
					<?php endif; ?>

					<!-- FEATURED CASE STUDEIS -->
					<?php if($featured_case_studies) : ?>
						<div class="section-header">
							<?php 
								if(get_current_blog_id() == 2) {
									echo '<h2>FEATURED WORK</h2>';
								} else {
									echo '<h2>FEATURED CASE STUDIES<div class="underline"></div></h2>';
								}
							?>
						</div>
						
						<div class="cfm-project-gallery">
							<ul>
								<?php foreach($projects as $key => $project) : ?>
									<li data-image="<?php echo $project['gallery_thumbnail']['url']; ?>">
										<div class="project-inner">
											<a class="cfm-project bg_image_view ready" href="<?php echo $project['permalink']; ?>" style="background-image: url(<?php echo $project['gallery_thumbnail']['url']; ?>);">
												<div class="project-label"><div class="project-label-inner"><h2><?php echo $project['title']; ?></h2></div></div>
											</a>
										</div>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					<?php endif; ?>

					<!-- CLIENTS -->
					<?php if($clients) : ?>
						<div class="section-header">
							<h2>CLIENTS<div class="underline"></div></h2>
						</div>
						<div id="clients-slider">
					        <div id="client-carousel" class="owl-carousel">
					        	<?php foreach ($clients as $key => $client): 
					        		if ($key % 2 == 0) { ?>
					        			<div class="item">
					    					<img src="<?php echo $client['image']['url']; ?>" style="margin-bottom:10px;">
					        			<?php } else { ?>
					        				<img src="<?php echo $client['image']['url']; ?>">
					        			</div>
					        		<?php } ?>
								<?php endforeach; ?>
					        </div>
						</div>
					<?php endif; ?>


					<!-- REEL VIDEO -->
					<?php if($reel_video) : ?>
						<div id="reel-videoplayer" class="cfm-videoplayer" data-video-name="<?php echo $reel_video['url']; ?>" data-poster="<?php echo $reel_poster['url']; ?>">
						    <div class="cfm-videoplayer-inner">
					            <video class="cfm-videoplayer-mobile" width="960" height="540" controls></video>
					            <div class="cfm-videoplayer-poster">
					            	<div class="cfm-videoplayer-poster-inner">
						            	<div class="cfm-videoplayer-playbutton"><span class="arrow"></span></div>
						            </div>
					            </div>
					            <video class="cfm-videoplayer-desktop" width="960" height="540" loop src="<?php echo $reel_video['url']; ?>" style="height: 100%; width: auto;"></video>
					            <div class="cfm-video-controls">
					            	<ul>
										<li class="cfm-video-play-pause-btn cfm-video-btn"></li>
										<li class="cfm-video-fullscreen-btn cfm-video-btn"></li>
										<li class="cfm-video-mute-btn cfm-video-btn"></li>
									</ul><div class="cfm-video-progress-container"><a class="cfm-video-seek-bar"></a><input class="cfm-video-seek-bar-input" type="range" value="0"></div>
								</div>
						    </div>
						</div>
					<?php endif; ?>


				<?php endwhile; endif; ?>

			</div>

		</div>

<?php get_footer(); ?>