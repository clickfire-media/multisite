<?php get_header(); ?>

		<div id="page-container">
        	 
        	<div id="page-content">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="title-label"><h3><?php the_title(); ?></h3></div>

					<?php the_content(); ?>
				
				<?php endwhile; endif; ?>

			</div>

		</div>

<?php get_footer(); ?>