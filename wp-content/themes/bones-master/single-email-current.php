<?php // get_header(); ?>

<?php 


	// GET EMAIL TEMPLATE (TAXONOMY)
	$terms = get_the_terms($post->ID, 'email_template');
	if($terms) {
		$template = $terms[0]->slug;	
	} else {
		$template = 'click-3x';
	}


		// EMAIL FIELDS
	$title = get_the_title();
	$p_projects = get_field('projects');
	$projects = array();
	$files_to_zip = array();
	foreach ($p_projects as $key => $project) {
		$id = $project->ID;
		$my_project = array();
		$my_project['title'] = $project->post_title;
		$my_project['sub_title'] = get_field('sub_title', $id);
		$my_project['description'] = get_field('description', $id);
		$my_project['link'] = get_field('link', $id);
		$my_project['additional_links'] = get_field('additional_links', $id);
		$my_project['thumbnail'] = get_field('thumbnail', $id);

		// $img_file = $my_project['thumbnail']['url'];
		// // $img_file = str_replace($blog_url, $path, $img_file);
		// $img_file = basename($img_file);
		// array_push($files_to_zip, $img_file);
		array_push($projects, $my_project);
	}

	if($template == 'stoked') {
			$e_body = printHeader_stoked($title);
			foreach ($projects as $key => $project) {
				$e_body .= buildBlock_stoked($project);
			}
			$e_body .= printFooter_stoked();
		} else {
			$e_body = printHeader_cfm($title);
			foreach ($projects as $key => $project) {
				$e_body .= buildBlock_cfm($project);
			}
			$e_body .= printFooter_cfm();
		}

		// zip location in theme file
		// $ziploc = $file_path.'/php/email-archive/my-archive.zip';
		// $ziploc = $file_path.'/php/email-archive/my-archive.zip';
		// // create zip
		// $over_write = true;
		// $zip = create_zip($files_to_zip, $ziploc, $over_write);

		// helper($zip);
		// echo '<h1>Zip: '.$zip.'</h1>';
		// create index.html
		// $indexloc = $file_path.'/php/email-archive/index.html';
		// $index = fopen($indexloc,"w");
		// fwrite($index,$e_body);
		// fclose($index);
		// attachments
		// $attachments = array($indexloc, $ziploc );
		// email fields
		$to = get_field('to');
		$subject = get_field('subject');
		$headers = array('Content-Type: text/html; charset=UTF-8');
		// put it all together & send email
	    // wp_mail( $to, $subject, $e_body, $headers, $attachments );
	    // wp_mail( $to, $subject, $e_body, $headers );

	     // wp_mail( $to, $subject, $e_body, $headers );

	    echo $e_body;

	// helper($projects);

	// $e_body = printHeader($title);

	// foreach ($projects as $key => $project) {
	// 	$e_body .= buildBlock($project);	
	// }

	// $e_body .= printFooter();

	// $e_body = file_get_contents(get_bloginfo('template_url').'/php/resp-email.html');
	
	// email
	// $to = get_field('to');
	// $subject = get_field('subject');
	// $headers = array('Content-Type: text/html; charset=UTF-8');

	// echo $e_body;

    

    wp_mail( $to, $subject, $e_body, $headers );
?>

<?php // get_footer(); ?>