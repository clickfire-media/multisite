<?php 
/*

Template Name: Click3x-Directors
*/

get_header();

$args = array(
	'post_type'   		=> 'director',
	'posts_per_page' 	=> -1,
);

$query = new WP_Query( $args );	

 ?>

		<div id="page-container">
        	 
        	<div id="page-content">
				
				<div id="directors-page" class="page-content-inner">

					<a class="x3-logo" href="<?php echo get_bloginfo('url'); ?>"><svg version="1.1" id="logo-container" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 710 580" enable-background="new 0 0 710 580" xml:space="preserve">
					    <g>
					        <path id="number-3" fill="#B31F2E" stroke="#B31F2E" stroke-width="3" stroke-miterlimit="10" d="M480.4,177.9
					            c26.2,0,47.5-12.5,51.5-36.9c4.5-26.8-13.2-39.6-41-39.6H454L514.3,24H424V3.3h120.9v16.9l-50.5,63.3c41,0,63.8,20.9,59.4,58.7
					            c-2.9,24.8-22.9,60.2-88.3,55.3L480.4,177.9z"></path>
					        <path id="black-x" stroke="#000000" stroke-width="3" stroke-miterlimit="10" d="M232.7,418.9h-25.7L323,267.8L201.7,111.7h25.6
					            l121.3,156.1L232.7,418.9z"></path>
					        <polygon id="red-x" fill="#B31F2E" stroke="#B31F2E" stroke-width="3" stroke-miterlimit="10" points="467.3,117.4 441.3,117.4 
					            344.9,242.2 485.7,424.6 511.4,424.6 370.5,241.9     "></polygon>
					        <g id="text">
					            <path d="M24.7,578.1c-6.3,0-12-2.3-16.3-6.6c-6-6-6-12.3-6-25.8c0-13.5,0-19.8,6-25.8c4.3-4.3,10-6.6,16.3-6.6
					                c11.5,0,20,7,22.1,19h-7c-1.7-7.8-7.2-12.9-15.1-12.9c-4.2,0-8.2,1.6-11,4.5c-4,4.1-4.5,8.5-4.5,21.8s0.4,17.7,4.5,21.8
					                c2.8,2.9,6.8,4.5,11,4.5c8,0,13.6-5.1,15.3-12.9h6.8C44.7,571.1,36,578.1,24.7,578.1z"></path>
					            <path d="M68.6,577.5v-63.7h6.8v57.7h32.6v6.1H68.6z"></path>
					            <path d="M126.6,577.5v-63.7h6.8v63.7H126.6z"></path>
					            <path d="M178.4,578.1c-6.3,0-12-2.3-16.3-6.6c-6-6-6-12.3-6-25.8c0-13.5,0-19.8,6-25.8c4.3-4.3,10-6.6,16.3-6.6
					                c11.5,0,20,7,22.1,19h-7c-1.7-7.8-7.2-12.9-15.1-12.9c-4.2,0-8.2,1.6-11,4.5c-4,4.1-4.5,8.5-4.5,21.8s0.4,17.7,4.5,21.8
					                c2.8,2.9,6.8,4.5,11,4.5c8,0,13.6-5.1,15.3-12.9h6.8C198.5,571.1,189.8,578.1,178.4,578.1z"></path>
					            <path d="M261.9,577.5l-19.4-33.9l-13.4,16.1v17.8h-6.8v-63.7h6.8v36.6l29.7-36.6h8.3l-20.1,24.6l22.9,39.1H261.9z"></path>
					            <path d="M315.6,578.2c-10,0-18.2-5.3-18.7-16.8h6.4c0.5,8,6.2,11.1,12.3,11.1c6.9,0,12.3-4.6,12.3-12.4c0-7.7-3.8-12.4-12.5-12.4
					                H314v-5.6h1.3c7.8,0,11.4-4.4,11.4-11.5c0-7.6-4.9-11.6-11.1-11.6c-6.7,0-10.8,4.1-11.5,10.7h-6.4c0.7-10.3,8.2-16.4,17.9-16.4
					                c10.4,0,17.5,6.9,17.5,17.3c0,6.7-2.9,11.5-8.6,14.1c6.4,2.4,9.8,7.6,9.8,15.6C334.3,571.9,325.9,578.2,315.6,578.2z"></path>
					            <path d="M386.6,577.5l-15.5-26.8l-15.6,26.8h-7.7l19.5-32.7l-18.3-31.1h7.9l14.1,25.2l14.1-25.2h7.9l-18.4,31.1l19.7,32.7H386.6z"></path>
					            <path d="M447.6,519.9v23.5h27.9v6.1h-27.9v28.1h-6.8v-63.7h39.6v6.1H447.6z"></path>
					            <path d="M499.5,577.5v-63.7h6.8v63.7H499.5z"></path>
					            <path d="M531.4,577.5v-63.7h6.8v57.7h32.6v6.1H531.4z"></path>
					            <path d="M637.2,577.5v-48.4l-17.5,38.4H614l-17.7-38.4v48.4h-6.8v-63.7h6.8L617,559l20.2-45.2h6.8v63.7H637.2z"></path>
					            <path d="M685.6,578.1c-9.6,0-15.8-2.4-21.8-8.4l4.7-4.7c5.3,5.3,10.2,7,17.4,7c9.3,0,15-4.4,15-11.8c0-3.3-1-6.2-3-8
					                c-2.1-1.8-3.7-2.3-8.3-3L682,548c-5-0.8-9-2.4-11.7-4.8c-3.2-2.9-4.8-6.8-4.8-11.9c0-10.8,7.8-18,20.4-18c8.1,0,13.5,2.1,19,7
					                l-4.4,4.4c-3.9-3.6-8.1-5.5-14.9-5.5c-8.5,0-13.4,4.7-13.4,11.8c0,3.1,0.9,5.6,2.9,7.3c2,1.7,5.3,3,8.6,3.5l7,1.1
					                c6,0.9,9,2.1,11.6,4.5c3.5,3,5.4,7.3,5.4,12.6C707.6,571.2,698.8,578.1,685.6,578.1z"></path>
					        </g>
					    </g>
					</svg></a>
					<?php 
						// TITLE
						echo '<div class="title-label"><h3>'.get_the_title().'</h3></div>';

						// HEADLINE + HEADLINE COPY
						// echo '<div class="page-header"><div class="page-header-inner">';
						// 	if(get_field('headline')) { echo '<h1>'.get_field('headline').'</h1>'; }
						// 	if(get_field('headline_copy')) { echo get_field('headline_copy'); }
						// echo '</div></div>';
					?>

					<?php 
						// PROJECT GALLERY
						echo '<div class="cfm-project-gallery">';
							echo '<ul>';
								if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
									// helper($post);
									$id = $post->ID;
									$name = $post->post_title;
									$slug = $post->post_name;
									$link = $post->guid;
									if(get_field('gallery_image', $id)) {
										$img = get_field('gallery_image', $id);
									} else {
										$img = '';
									}

									echo '<li data-id="'.$id.'" data-image="'.$img['url'].'">';
										echo '<div class="project-inner hoverfriend">';
											echo '<a class="cfm-project bg_image_view ready" href="'.$link.'" style="background-image: url('.$img['url'].');">';
												echo '<div class="red-gradient"></div>';
											echo '</a>';
											echo '<div class="project-label"><div class="project-label-inner"><div class="label-line"></div><h2>'.$name.'</h2></div></div>';
										echo '</div>';
									echo '</li>';
								endwhile; endif; 

							echo '</ul>';
						echo '</div>';
					?>

				</div>

			</div>

		</div>

<?php get_footer(); ?>