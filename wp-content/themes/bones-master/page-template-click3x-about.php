<?php 
/*

Template Name: Click3x-about

*/

get_header(); ?>

		<div id="page-container">
        	 
        	<div id="page-content">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<div id="about-page" class="page-content-inner">
						<?php 
							// TITLE
							echo '<div class="title-label"><h3>'.get_the_title().'</h3></div>';

							// HEADLINE + HEADLINE COPY
							echo '<div class="page-header"><div class="page-header-inner">';
								if(get_field('headline')) { echo '<h1>'.get_field('headline').'</h1>'; }
								if(get_field('headline_copy')) { echo get_field('headline_copy'); }
							echo '</div></div>';
						?>

						<?php 
							$copy_images_module = get_field('copy_images_module');
							$img_sizes = array('crew-container', 'double', 'triple');

							if($copy_images_module) {
								foreach ($copy_images_module as $key => $module) {
									// helper($module);
									$images = $module['images'];
									$copy = $module['copy'];

									if(isset($image['class'])) {
										$class = $image['class'];	
									} else {
										$class = 'mod-img';
									}
									

									if(!empty($images)) {
										$ct = count($images);
										echo '<div class="clearfix bg-img-container '.$img_sizes[$ct-1].'">';
											foreach ($images as $key => $image) {
												echo '<div class="'.$class.'" style="background-image:url('.$image['image']['url'].');"></div>';
											}
										echo '</div>';
									}

									if($copy) {
										echo '<div class="page-header"><div class="page-header-inner">'.$copy.'</div></div>';
									}
									
										

								}	
							}

						?>

					</div>
				
				<?php endwhile; endif; ?>

			</div>

		</div>

<?php get_footer(); ?>