<?php // get_header(); ?>

<?php 
	$path = getcwd();
	$blog_url = get_bloginfo('url');
	$p_projects = get_field('projects');
	$title = get_the_title();
	$file_path = $path.'/wp-content/themes/bones-master';
	$projects = array();
	$files_to_zip = array();
	$img_size = '';

	if(get_field('footer_content')) {
		$footer_content = get_field('footer_content');
	} else {
		$footer_content = '';	
	}
	

	// GET EMAIL TEMPLATE (TAXONOMY)
	$terms = get_the_terms($post->ID, 'email_template');
	if($terms) {
		$template = $terms[0]->slug;	
	} else {
		$template = 'click-3x';
	}

	if($template == 'stoked') {
		$add_images = array(
			get_bloginfo('url').'/email-images/stoked.png',
			get_bloginfo('url').'/email-images/stoked-logo.png'
		);
		$img_size = 'stoked-email';
	} elseif($template == 'click-3x') {
		$add_images = array(
			get_bloginfo('url').'/email-images/click-logo.png',
			get_bloginfo('url').'/email-images/cfm-logo.png',
			get_bloginfo('url').'/email-images/bg5.jpg',
			get_bloginfo('url').'/email-images/E-mail_Footer.jpg',
			get_bloginfo('url').'/email-images/tw.png',
			get_bloginfo('url').'/email-images/fb.png',
			get_bloginfo('url').'/email-images/in.png'
		);
		$img_size = 'cfm-email';
	}

	foreach ($add_images as $key => $add_image) {
		$add_image = str_replace($blog_url, $path, $add_image);
		array_push($files_to_zip, $add_image);
	}

	// EMAIL FIELDS
	foreach ($p_projects as $key => $project) {
		$id = $project->ID;
		$my_project = array();
		$my_project['title'] = $project->post_title;
		$my_project['sub_title'] = get_field('sub_title', $id);
		$my_project['description'] = get_field('description', $id);
		$my_project['link'] = get_field('link', $id);
		$my_project['additional_links'] = get_field('additional_links', $id);
		$my_project['thumbnail'] = get_field('thumbnail', $id);

		// $img_file = $my_project['thumbnail']['url'];
		$img_file = $my_project['thumbnail']['sizes'][$img_size];
		$img_file = str_replace($blog_url, $path, $img_file);
		array_push($files_to_zip, $img_file);
		array_push($projects, $my_project);
	}

	$sendemail = false;

	// IF SEND EMAIL GET VARIABLE IS SET = BUILD EMAIL AND SEND IT
	if(isset($_GET["sendemail"])) {
		$sendemail = true;
		// build email
		if($template == 'stoked') {
			$index_body = printHeader_stoked($title, $sendemail);
			$e_body = printHeader_stoked($title, false);
			foreach ($projects as $key => $project) {
				$index_body .= buildBlock_stoked($project, $sendemail);
				$e_body .= buildBlock_stoked($project, false);
			}
			$index_body .= printFooter_stoked($project, $sendemail);
			$e_body .= printFooter_stoked($project, false);
		} else {
			$index_body = printHeader_cfm($title, $sendemail);
			$e_body = printHeader_cfm($title, false);
			foreach ($projects as $key => $project) {
				$index_body .= buildBlock_cfm($project, $sendemail);
				$e_body .= buildBlock_cfm($project, false);
			}
			$index_body .= printFooter_cfm($project, $footer_content, $sendemail);
			$e_body .= printFooter_cfm($project, $footer_content, false);
		}


		// email fields
		$to = get_field('to');
		$subject = get_field('subject');
		$headers = array('Content-Type: text/html; charset=UTF-8');


		if(isset($_GET["sendassets"])) {
			// zip location in theme file
			$ziploc = $file_path.'/php/email-archive/img.zip';
			// create zip
			$over_write = true;
			$zip = create_zip($files_to_zip, $ziploc, $over_write);
			// create index.html
			$indexloc = $file_path.'/php/email-archive/index.html';
			$index = fopen($indexloc,"w");
			fwrite($index,$index_body);
			fclose($index);
			// attachments
			$attachments = array($indexloc, $ziploc );
			// put it all together & send email
		    wp_mail( $to, $subject, $e_body, $headers, $attachments );
		} else {
			// put it all together & send email
		    wp_mail( $to, $subject, $e_body, $headers );
		}
	}


	$sendemail = false;
	// PRINT EMAIL TO SCREEN
	if($template == 'stoked') {
		// $body = printNoHeader_stoked($title);
		$body = printHeader_stoked($title, $sendemail);
		foreach ($projects as $key => $project) {
			$body .= buildBlock_stoked($project, $sendemail);
		}
		if(isset($_GET["sendemail"])) {
			$body .= '<a id="send-email-btn" class="send-email-btn email-sent" style="text-align:left; position:fixed; bottom: 50px; left:50px; display:inline-block; padding:20px; background-color:rgba(0,0,0,0.65); color:white; text-transform:uppercase;">Test Email sent to: '.get_field('to').'<br><br><span style="color:red; text-transform:none; font-size:0.8em;">If your email doesn\'t immediately appear,<br>check your SPAM inbox.</span></a>';
		} else {
			$body .= '<div style="text-align:left; position:fixed; bottom: 50px; left:50px; display:inline-block; padding:20px; background-color:rgba(0,0,0,0.65); color:white; text-transform:uppercase;">';
				$body .= '<a id="send-email-btn" class="send-email-btn" href="'.get_the_permalink().'?sendemail=true">Send Test Email</a><br><br>';
				$body .= '<a id="send-email-btn" class="send-email-btn" href="'.get_the_permalink().'?sendemail=true&sendassets=true">Include attachments for MailChimp</a>';
			$body .= '</div>';
		}
		// $body .= printNoFooter_stoked($project);
		$body .= printFooter_stoked($project, $sendemail);
	} else {
		$body = printHeader_cfm($title, $sendemail);
		foreach ($projects as $key => $project) {
			$body .= buildBlock_cfm($project, $sendemail);
		}
		if(isset($_GET["sendemail"])) {
			$body .= '<a id="send-email-btn" class="send-email-btn email-sent" style="text-align:left; position:fixed; bottom: 50px; left:50px; display:inline-block; padding:20px; background-color:rgba(0,0,0,0.65); color:white; text-transform:uppercase;">Test Email sent to: '.get_field('to').'<br><br><span style="color:red;">Check Spam Inbox</span></a>';
		} else {
			// $body .= '<a id="send-email-btn" class="send-email-btn" href="'.get_the_permalink().'?sendemail=true" style="position:fixed; bottom: 50px; left:50px; display:inline-block; padding:20px; background-color:rgba(0,0,0,0.65); color:white; text-transform:uppercase;">Send Test Email</a>';
			$body .= '<div style="text-align:left; position:fixed; bottom: 50px; left:50px; display:inline-block; padding:20px; background-color:rgba(0,0,0,0.65); color:white; text-transform:uppercase;">';
				$body .= '<a id="send-email-btn" class="send-email-btn" href="'.get_the_permalink().'?sendemail=true">Send Test Email</a><br><br>';
				$body .= '<a id="send-email-btn" class="send-email-btn" href="'.get_the_permalink().'?sendemail=true&sendassets=true">Include attachments for MailChimp</a>';
			$body .= '</div>';
		}
		$body .= printFooter_cfm($project, $footer_content, $sendemail);
	}
	echo $body;
?>

<?php // get_footer(); ?>