<?php 

/*

Template Name: R2B Contact

*/

get_header(); 

?>

		<div id="page-container">
        	 
        	<div id="contact-page" class="page-content-inner">
				<div class="page-header">
					<div class="page-header-inner">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php the_content(); ?>
					
					<?php endwhile; endif; ?>
					</div>
				</div>

				<div class="cfm-google-map">
					<div id="map"></div>
				</div>
				
			</div>

		</div>

<?php get_footer(); ?>