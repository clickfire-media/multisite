<?php get_header(); ?>

			<div id="page-container">
        	 
        		<div id="project-page" class="page-content-inner">
					<div id="project-details-container">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							
							<div class="page-header-navigation">
								<div class="page-header-inner">
									<div class="cfm-project-naviation cfm-navigation">
										<ul>
											<li><a href="<?php echo get_permalink(39); ?>" data-navigate-to="home"><span class="line-arrow-left line-arrow"></span><h4>Home</h4></a></li>
											<?php
												$prev_post = get_previous_post();
												if($prev_post) {
												   $prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));
												   echo "\t" . '<li class="previous-button"><a rel="prev" href="' . get_permalink($prev_post->ID) . '" title="' . $prev_title. '" class=" "><span class="line-arrow-left line-arrow"></span></a></li>' . "\n";
												}

												$next_post = get_next_post();
												if($next_post) {
												   $next_title = strip_tags(str_replace('"', '', $next_post->post_title));
												   echo "\t" . '<li class="next-button"><a rel="next" href="' . get_permalink($next_post->ID) . '" title="' . $next_title. '" class=" "><span class="line-arrow-right line-arrow"></span></a></li>' . "\n";
												}
											?>
										</ul>
									</div>
								</div>
							</div><!--  /.page-header-navigation -->

							<?php
								get_template_part( 'post-formats/format-project', get_post_format() );
							?>

							<div class="page-footer-navigation">
								<div class="page-footer-inner">
									<div class="cfm-project-naviation cfm-navigation">
										<ul>
											<li><a href="<?php echo get_permalink(39); ?>" data-navigate-to="home"><span class="line-arrow-left line-arrow"></span><h4>Home</h4></a></li>
											<?php
												$prev_post = get_previous_post();
												if($prev_post) {
												   $prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));
												   echo "\t" . '<li class="previous-button"><a rel="prev" href="' . get_permalink($prev_post->ID) . '" title="' . $prev_title. '" class=" "><span class="line-arrow-left line-arrow"></span></a></li>' . "\n";
												}

												$next_post = get_next_post();
												if($next_post) {
												   $next_title = strip_tags(str_replace('"', '', $next_post->post_title));
												   echo "\t" . '<li class="next-button"><a rel="next" href="' . get_permalink($next_post->ID) . '" title="' . $next_title. '" class=" "><span class="line-arrow-right line-arrow"></span></a></li>' . "\n";
												}
											?>
										</ul>
									</div>
								</div>
							</div><!--   /.page-footer-navigation -->

						<?php endwhile; ?>

						<?php endif; ?>

					</div>
				</div>

			</div>

<?php get_footer(); ?>
