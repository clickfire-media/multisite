console.log('DirectorVIEW js!!!');
function DIRECTORVIEW (directorView) {
  var directorView = directorView || {};


  directorView.id = "director";
  directorView.videos = [];
  directorView.banners = [];

  var ovclose_btn,
    directorSwiper;

  // directorView.onready = function(){
  //   this.details_container_el = this.$el.find("#director-details-container").eq(0);
  //   this.initdetails();
  // };

  ///////
  directorView.initialize = function(options){
    var _t = this;
    _t.$el = jQuery(options.el);
    _t.details_container_el = this.$el.find("#director-details-container").eq(0);


    ovclose_btn = this.$el.find('.overlay-close');

    

    ovclose_btn.click(function(){
      _t.resetothervideos();
      _t.closeOverlay();
    });


    _t.disableHover();
    _t.overlayCloseMobile(ovclose_btn);
    _t.viewMore();

    // init overlay slider
    directorSwiper = new Swiper ('.directorOverlay-swiper-container', {
      direction: 'horizontal',
      loop: true,
      slidesPerView: 1,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      onSlideChangeStart: function(swiper) {
        if (jQuery('.video-overlay').hasClass('open') ) {
          console.log('slide changes');
          _t.resetothervideos();

          if (!jQuery('.swiper-slide-active').find("video").attr('src')) {
            // load next video if not exist
            _t.buildVideo(jQuery('.swiper-slide-active').find(".cfm-videoplayer"));

          } else {
            console.log('the video has already been loaded.');
          }

          if(!mobile) {
              var cur_slide = directorSwiper.slides[directorSwiper.activeIndex];
              jQuery(cur_slide).find('video')[0].play();
          }
        }
      }
    });

    _t.$el.find('.director-item').click(function(){

      if (jQuery(this).attr('id') != 'reel-video') {

        var module_id = jQuery(this).data('id');
        // var module_id = jQuery(this).attr('id');
        jQuery('.swiper-slide').removeClass('swiper-slide-active');
        jQuery('#directorOverlay-slide-' + module_id).addClass('swiper-slide-active');
        directorSwiper.slideTo( jQuery('.swiper-slide-active').index(),500,false );

        console.log('This is module_id: ', module_id);


        if (!jQuery('.swiper-slide-active').find("video").attr('src')) {
          // build videos
          _t.buildVideo(jQuery('.swiper-slide-active').find(".cfm-videoplayer"));


        } else {
          console.log('the video has already been loaded.');
        }


        // autoplay video (if !mobile)
        if(!mobile) {
          var filename = jQuery(this).data('filename');
          console.dir('This is filename: ', filename);
          // jQuery('#director-video-' + filename).find('video')[0].play();
          // jQuery('#directorOverlay-slide-' + module_id).find('video')[0].play();
          jQuery('#director-video-' + module_id).find('video')[0].play();
        
          console.log('not mobile + not view reel');
        }

        jQuery('#director-overlay-container').addClass('open');
        jQuery('html').addClass('noScroll');

      } else {

        if(!jQuery('html').hasClass('allmobile')) {

          console.log('not mobile + view reel');

          // build first video
          if (!jQuery('.swiper-slide-active').find("video").attr('src')) {
            // build videos
            _t.buildVideo(jQuery('.swiper-slide-active').find(".cfm-videoplayer"));

          } else {
            console.log('the video has already been loaded.');
          }

          // loop all the videos starting from the first one
          var fvideo = jQuery('.swiper-slide-active').find('video')[0];
          fvideo.play();

          var videoArray = jQuery('.swiper-slide').find('video');

          for (i = 0; i < videoArray.length; i++) {

            videoArray[i].onended = function(e) {
                console.log('video ends!');
                directorSwiper.slideNext();
              };
          }

          jQuery('#director-overlay-container').addClass('open');
          jQuery('html').addClass('noScroll');
        }
      }
    }); 


  };

  directorView.viewMore = function() {
    var _t = this;
    _t.details_container_el.find('.view-more').click(function(){
      jQuery('.hidden-bio').slideToggle();
      jQuery(this).children('span').text(function(i, text){
          return text === "MORE" ? "LESS" : "MORE";
        });
    });
  };

  directorView.buildVideo = function(cur_video) {
    var _t = this;
    var reel_video = cur_video;

    console.log('This is _t from buildVideo:', _t);
    console.log('This is reel_video from buildVideo:', reel_video);

    var url = reel_video.data('video-name');
    var poster = reel_video.data('poster');
    var video = new VIDEOVIEW;
    VIDEOVIEW.initialize({el:reel_video});
    url += mp4 ? ".mp4" : ".webm";
    video.load( url, mp4 ? "mp4" : "webm", poster );

    if (cur_video.attr('id') != 'reel-video') {
      _t.videos.push(video);
    } 
  };

  directorView.loadReelMobile = function() {
    jQuery('#reel-video').css('display', 'none');
    jQuery('#reel-static-container').css('display', 'block');
  };

  directorView.disableHover = function() {
    if(jQuery('html').hasClass('allmobile')) {
      jQuery('.project-inner').removeClass('hoverfriend');
    }
  };

  directorView.overlayCloseMobile = function(btn) {
    if(mobile) {
      btn.css({
        'top':'4px',
        'right':'10px',
        'background-position': '-0px -80px',
        'opacity': '1',
        '-webkit-transform': 'scale(0.6)',
        'transform': 'scale(0.6)'
      });
    }
  };

  directorView.resetothervideos = function(){
    if(!mobile) {
      jQuery.each(this.videos,function(i,v){
        v.reset();
      });
    }
  };

  directorView.closeOverlay = function() {
    jQuery('.video-overlay').removeClass('open');
    jQuery('html').removeClass('noScroll');
  };

  directorView.onclose = function(){
    jQuery.each(this.videos,function(i,v){
      v.remove();
    });
  };
  
  return directorView;
}