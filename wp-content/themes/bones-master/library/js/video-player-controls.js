// var VIDEOCONTROLS = (function(videoControls) {
function VIDEOCONTROLS (videoControls) {
    var videoControls = videoControls || {};

    videoControls.hasplayed = false;
    videoControls.initialize = function(options){
        this.video_el                    = options.video_el;
        this.$el                         = jQuery(options.video_el);
        this.$parent                      = jQuery(options.video_el).parent();
        this.el                         = this.$parent.find(".cfm-video-controls")[0];

        this.playButton                  = this.$parent.find( ".cfm-video-play-pause-btn")[0];
        this.muteButton                  = this.$parent.find( ".cfm-video-mute-btn")[0];
        this.fullScreenButton            = this.$parent.find( ".cfm-video-fullscreen-btn")[0];
        this.progressBarContainer        = this.$parent.find( ".cfm-video-progress-container")[0];
        this.seekBar                     = this.$parent.find( ".cfm-video-seek-bar")[0];
        this.seekBarInput                = this.$parent.find( ".cfm-video-seek-bar-input")[0];
        this.buffer                      = this.$parent.find( ".cfm-video-buffer-icon")[0];

        this.ready();
    };
    videoControls.ready = function(){
        var _t = this;

        // Event listener for the play/pause button
        jQuery(_t.playButton).mousedown(function() {
            if (_t.video_el.paused == true) {
                _t.video_el.play();
            } else {
                _t.video_el.pause();
            }
        });

        // Event listener for the mute button
        jQuery(_t.muteButton).click(function() {
            if (_t.video_el.muted == false) {
                _t.video_el.muted = true;
            } else {
                _t.video_el.muted = false;
            }

            _t.muteButton.classList.toggle('active');
        });

        // Event listener for the full-screen button
        jQuery(_t.fullScreenButton).click(function() {
            if( _t.video_el.requestFullscreen ){
                _t.video_el.requestFullscreen();
            }else if( _t.video_el.mozRequestFullScreen ){
                _t.video_el.mozRequestFullScreen(); // Firefox
            }else if( _t.video_el.webkitRequestFullscreen ){
                _t.video_el.webkitRequestFullscreen(); // Chrome and Safari
            }
        });

        // Event listener for the seek bar
        jQuery(_t.seekBarInput).on("change", function() {
            var time = _t.video_el.duration * (_t.seekBarInput.value / 100);
            _t.video_el.currentTime = time;
        });

        jQuery(_t.video_el).on("mousemove", function() {
            clearTimeout(_t.hidecontrolsto);
            clearTimeout(_t.showcontrolsto);

            _t.showcontrolsto = setTimeout( function(){
                _t.showControls();
            }, 50);
            
            _t.hidecontrolsto = setTimeout( function(){
                _t.hideControls();
            }, 1000);
        });

        jQuery(_t.video_el).on("mouseout", function() {
            clearTimeout(_t.hidecontrolsto);
            clearTimeout(_t.showcontrolsto);

            _t.hidecontrolsto = setTimeout( function(){
                _t.hideControls();
            }, 50);
        });

        jQuery(_t.el).on("mouseout", function() {
            clearTimeout(_t.hidecontrolsto);
            clearTimeout(_t.showcontrolsto);

            _t.hidecontrolsto = setTimeout( function(){
                _t.hideControls();
            }, 50);
        });

        jQuery(_t.el).on("mousemove", function() {
            clearTimeout(_t.hidecontrolsto);
            clearTimeout(_t.showcontrolsto);
            _t.showControls();
        });
    };
    videoControls.reset = function(){
        this.hasplayed          = false;
        this.playing            = false;
        this.el.style.opacity   = "0";
    };
    videoControls.onprogress = function(_value){
        this.progressBarContainer.style.backgroundSize = _value + "% 40px";
    };
    videoControls.ontimeupdate = function(_value){
        this.seekBar.style.width = _value + "%";
    };
    videoControls.toPlayingState = function(){
        this.playing = true;
        this.hasplayed = true;

        if( !jQuery(this.playButton).hasClass("active") )
            jQuery(this.playButton).addClass("active");

        if(ipad == true) this.showControls();
    };
    videoControls.toPausedState = function(){
        this.playing = false;

        jQuery(this.playButton).removeClass("active");

        this.showControls();
    };
    videoControls.showBuffering = function(){
        this.buffer.style.opacity = ".5";
    };
    videoControls.hideBuffering = function(){
        this.buffer.style.opacity = "0";
    };
    videoControls.showControls = function(){
        console.log("show");
        if( this.hasplayed && !mobile ) {
            this.el.style.opacity = ".75";   
        }
    };
    videoControls.hideControls = function(){
        if(this.playing && ipad == false)
            this.el.style.opacity = "0";
    };

    return videoControls;
}