jQuery(document).ready(function($) {

  console.log('This is video-player-init.js!');

  // STORE VIDS
  var $cfmVids = $(".cfm-videoplayer");
  // INIT VIDS
  $.each($cfmVids, function(){
    var url = this.getAttribute("data-video-name"),
    poster = this.getAttribute("data-poster"),
    video = new VIDEOVIEW;
    video.initialize({el:this});
    
    video.load( url, mp4 ? "mp4" : "webm", poster );
  });




  // STORE bg_imgs
  var $bgImgs = $('[data-image]');
  // INIT VIDS
  $.each($bgImgs, function(){
    // console.log('This is bgImg: ', this);
    var _t = this;
    var img = $(this).data('image');
    _t.bg = new BGIMAGEVIEW;

    _t.bg.initialize({ el:this, image_url:img });
  });




    // WATCH VIDEO LINK CLICK
    var $iLinks = $('a[href*=#]'); 
    $iLinks.click(function(e){     
        e.preventDefault();
        $('body').animate( {scrollTop: ($(this.hash).offset().top-33) + "px"} , 500);
    });


});