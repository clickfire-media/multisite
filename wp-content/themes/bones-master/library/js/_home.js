define([
  'pages/page_view',
  'text!template/home.php',
  'modules/videoplayer/views/videoplayer_view'
], function(PageView, Template, VideoPlayerView){
  var HomeView = PageView.extend({
    videos:[],
    template: _.template( Template ),
    id:"home",
    onready:function(){
      var _t = this;
      var ovclose_btn = _t.$el.find('.overlay-close');
      var mute_btn = _t.$el.find('#speaker');
      var skip_btn = _t.$el.find('#skip-intro-btn');
      var homeSwiper, featuredSwiper;
      var gif_loaded = false;
      var homeSwiper_init = false;

      _t.overlayCloseMobile(ovclose_btn);     

      initHomeCarousel();

      $(window).resize(function(){
          $('.featured-poster').height(window.innerHeight * 0.95);
      });

      ovclose_btn.click(function(){
        _t.resetothervideos();
        _t.closeOverlay();
      });

      this.buildprojectgalleries();
      initFeaturedCarousel();
      
      _t.$el.find('.featured-item').click(function(){
        var featured_id = $(this).data('id');
        $('.swiper-slide').removeClass('swiper-slide-active');
        $('#featuredOverlay-slide-' + featured_id).addClass('swiper-slide-active');
        featuredSwiper.slideTo( $('.swiper-slide-active').index(),500,false );

        if (!$('.swiper-slide-active').find("video").attr('src')) {
          // load this video if not exist
          _t.buildVideo($('.swiper-slide-active').find(".cfm-videoplayer"));          
        } 

        openOverlay();
        homeSwiper.stopAutoplay();

        // if not mobile auto start video
        if(!mobile) {
          var filename = $(this).data('filename');
          $('#featured-video-' + filename).find('video')[0].play();
        }

      });

      function initHomeCarousel() {
        homeSwiper = new Swiper ('.home-swiper-container', {
            direction: 'horizontal',
            loop: true,
            slidesPerView: 1,
            autoplay: 8000,
            speed: 1200,
            pagination: '.swiper-pagination',
            paginationClickable: true,

            onInit: function(swiper) {
          // Hide the initial overlays...
          jQuery("#shape-container").hide();
          jQuery("#shape-containerA").hide();
          jQuery("#shape-container2").hide();
          jQuery("#shape-container2A").hide();  
            },

            onSlideChangeStart: function(swiper) {
              // console.log('on slide change start');
              // begin red border highlight
              $('#company-container > div').removeClass('active-index');
          if((swiper.activeIndex == 1) || (swiper.activeIndex == 4) ){
                $('#company-container > div').eq(0).addClass('active-index');
              } else if(swiper.activeIndex == 2) {
                $('#company-container > div').eq(1).addClass('active-index');
              } else if((swiper.activeIndex == 3) || (swiper.activeIndex == 0) ){
                $('#company-container > div').eq(2).addClass('active-index');
              }
              // end red border highlight
              

              if ( !$('html').hasClass('allmobile') ) {
                if ( swiper.activeIndex % 2 == 0) {
              jQuery("#shape-container").show().animate({left: "-100%"}, 900);
              jQuery("#shape-container2").show().animate({left: "100%"}, 600);
            }
            else {
              jQuery("#shape-containerA").show().animate({left: "-100%"}, 900);
              jQuery("#shape-container2A").show().animate({left: "100%"}, 600);
            }
          }
            },

            onSlideChangeEnd: function(swiper) {
              // console.log('on slide change end');
              if (!$('html').hasClass('allmobile')) {

            if ( swiper.activeIndex % 2 == 0) {
              jQuery("#shape-container").hide().animate({left: "100%"}, 900);
              jQuery("#shape-container2").hide().animate({left: "-100%"}, 600);
            }
            else {
              jQuery("#shape-containerA").hide().animate({left: "100%"}, 900);
              jQuery("#shape-container2A").hide().animate({left: "-100%"}, 600);
            }

          }
            },

          });
      }

      function initFeaturedCarousel() {
        featuredSwiper = new Swiper ('.featuredOverlay-swiper-container', {
            direction: 'horizontal',
            loop: true,
            slidesPerView: 1,
            nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          onSlideChangeStart: function(swiper) {
            if ($('.video-overlay').hasClass('open') ) {
              // console.log('slide changes');
            _t.resetothervideos();

            if (!$('.swiper-slide-active').find("video").attr('src')) {           
              // load next video if not exist
              _t.buildVideo($('.swiper-slide-active').find(".cfm-videoplayer"));
            }

            // if not mobile auto start video
            if(!mobile) {
              var cur_slide = swiper.slides[swiper.activeIndex];
                $(cur_slide).find('video')[0].play();
              }
            }

          }
          }); 
      }

      function openOverlay() {
        $('#featured-overlay-container').addClass('open');
        $('html').addClass('noScroll');
      }
    },

    buildVideo:function(cur_video) {
      var reel_video = cur_video;
      var url = reel_video.data('video-name');
      var poster = reel_video.data('poster');
      var video = new VideoPlayerView({el:reel_video});
      url += mp4 ? ".mp4" : ".webm";
      video.load( url, mp4 ? "mp4" : "webm", poster );
      this.videos.push(video);
    },

    resetothervideos:function(){
      if(!mobile) {
        $.each(this.videos,function(i,v){
          v.reset();
        });
      }
    },

    closeOverlay:function() {     
      $('.video-overlay').removeClass('open');
      $('html').removeClass('noScroll');
    },

    loadIntroVideo:function() {
      var introvideo = document.createElement("video");
        introvideo.setAttribute("id", "introVideoElement");
        introvideo.setAttribute("src", "https://s3.amazonaws.com/media.xthreefilms.com/video/intro/intro.mp4");
        introvideo.setAttribute("autoplay", "autoplay");
        introvideo.setAttribute("preload", "auto");
        document.getElementById("introVideoContainer").insertBefore(introvideo, document.getElementById("skip-intro-btn") );
    },

    loadGif:function() {
      var introgif = document.createElement("img");
        introgif.setAttribute("id", "introVideoElement");
        document.getElementById("introVideoContainer").insertBefore(introgif, document.getElementById("skip-intro-btn") );
    },

    toggleMute:function(mute_btn) {
      mute_btn.click(function(){
        $('#introVideoElement').prop('muted', !$('#introVideoElement').prop('muted'));
        $(this).toggleClass('mute');
      });     
    },

    hideMute:function(mute_btn, skip_btn) {
      skip_btn.css('right','18px');
      mute_btn.css('display', 'none');
    },

    overlayCloseMobile: function(btn) {
      if(mobile) {
        btn.css({
          'top':'4px',
          'right':'10px',
          'background-position': '-0px -80px',
          'opacity': '1',
          '-webkit-transform': 'scale(0.6)',
          'transform': 'scale(0.6)'
        });
      }
    },

    onclose:function(){
      $.each(this.videos,function(i,v){
        v.remove();
      });
    },
  });
  return HomeView;
});