jQuery(document).ready(function($) {

    var _t = this;
    var ovclose_btn = $('.overlay-close').get(0);
    var mute_btn = $('#speaker');
    var skip_btn = $('#skip-intro-btn');
    var homeSwiper, featuredSwiper;
    var gif_loaded = false;
    var homeSwiper_init = false;

    console.log('This is home js!');
    function initHomeCarousel() {
        homeSwiper = new Swiper ('.home-swiper-container', {
            direction: 'horizontal',
            loop: true,
            slidesPerView: 1,
            autoplay: 8000,
            speed: 1200,
            pagination: '.swiper-pagination',
            paginationClickable: true,

            // onInit: function(swiper) {
            //     // Hide the initial overlays...
            //     jQuery("#shape-container").hide();
            //     jQuery("#shape-containerA").hide();
            //     jQuery("#shape-container2").hide();
            //     jQuery("#shape-container2A").hide();    
            // },

            // onSlideChangeStart: function(swiper) {
            //     // console.log('on slide change start');
            //     // begin red border highlight
            //     $('#company-container > div').removeClass('active-index');
            //     if((swiper.activeIndex == 1) || (swiper.activeIndex == 4) ){
            //         $('#company-container > div').eq(0).addClass('active-index');
            //     } else if(swiper.activeIndex == 2) {
            //         $('#company-container > div').eq(1).addClass('active-index');
            //     } else if((swiper.activeIndex == 3) || (swiper.activeIndex == 0) ){
            //         $('#company-container > div').eq(2).addClass('active-index');
            //     }
            //     // end red border highlight
                

            //     if ( !$('html').hasClass('allmobile') ) {
            //         if ( swiper.activeIndex % 2 == 0) {
            //             jQuery("#shape-container").show().animate({left: "-100%"}, 900);
            //             jQuery("#shape-container2").show().animate({left: "100%"}, 600);
            //         }
            //         else {
            //             jQuery("#shape-containerA").show().animate({left: "-100%"}, 900);
            //             jQuery("#shape-container2A").show().animate({left: "100%"}, 600);
            //         }
            //     }
            // },

            // onSlideChangeEnd: function(swiper) {
            //     // console.log('on slide change end');
            //     if (!$('html').hasClass('allmobile')) {

            //         if ( swiper.activeIndex % 2 == 0) {
            //             jQuery("#shape-container").hide().animate({left: "100%"}, 900);
            //             jQuery("#shape-container2").hide().animate({left: "-100%"}, 600);
            //         }
            //         else {
            //             jQuery("#shape-containerA").hide().animate({left: "100%"}, 900);
            //             jQuery("#shape-container2A").hide().animate({left: "-100%"}, 600);
            //         }

            //     }
            // }

        });
    }

    // initHomeCarousel();

    // homeSwiper = new Swiper ('.home-swiper-container', {
    //         direction: 'horizontal',
    //         loop: true,
    //         slidesPerView: 1,
    //         autoplay: 8000,
    //         speed: 1200,
    //         pagination: '.swiper-pagination',
    //         paginationClickable: true
    //     });


});