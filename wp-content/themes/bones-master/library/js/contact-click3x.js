jQuery(document).ready(function($) {

  console.log('Page Contact Click 3x!');
  
  var mapOptions = {
              zoom: 15,
              backgroundColor:"#b9c6cb",
              color: "#b4132d",
        scrollwheel: false,
        disableDoubleClickZoom: true,
              center: new google.maps.LatLng(40.7413197,-73.9913,18),
              streetViewControl: false,
              panControl: false,
        zoomControl: true,
        draggable:true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false,
              styles: [
                {
                  featureType:"administrative",
                  elementType:"all",
                  stylers:[
                    {visibility:"off"}
                  ]
                },
                {
                  featureType:"road",
                  elementType:"all",
                  stylers:[
                    {visibility:"simplified"},
                    {saturation:-100},
                    {lightness:10}
                  ]
                },
                {
                  'featureType': 'road',
                'elementType': 'labels.text',
                'stylers': [
                    {color:"#ff3e3a"},
                ]
                },
                {
                  'featureType': 'administrative.neighborhood',
                'elementType': 'labels',
                'stylers': [
                    {visibility:"on"},
                    
                ]
                },
                {
                  'featureType': 'administrative.neighborhood',
                'elementType': 'labels.text',
                'stylers': [
                    {color:"#676767"},
                    { "weight": 0.2 },
                  // { "gamma": 4.25 }
                ]
                },
                {
                  featureType:"water",
                  elementType:"all",
                  stylers:[
                    {visibility:"on"},
                    {color:"#b9c6cb"},
                    {lightness:0}
                  ]
                },
                {
                  featureType:"water",
                  elementType:"labels",
                  stylers:[
                    {visibility:"off"}
                  ]
                },
                {
                  featureType:"landscape.man_made",
                  elementType:"all",
                  stylers:[
                    {visibility:"off"}
                  ]
                },
                {
                  featureType:"landscape.natural",
                  elementType:"off",
                  stylers:[
                    {visibility:"simplified"},
                    {saturation:-100},
                    {lightness:0}
                  ]
                },  
                {
                  featureType:"poi",
                  elementType:"all",
                  stylers:[
                    {visibility:"off"}
                  ]
                },
                {
                  featureType:"transit",
                  elementType:"all",
                  stylers:[
                    {visibility:"off"}
                  ]
                }
              ]
          };

          var mapElement = document.getElementById('map');

        var markerLatLng = new google.maps.LatLng(40.7413197,-73.9913,18);

          var map = new google.maps.Map(mapElement, mapOptions);

          var image1 = {
        url: base_url + '/images/contact/marker-c3x.png',
        size: new google.maps.Size(64, 64),
        anchor: new google.maps.Point(22, 86),
        scaledSize: new google.maps.Size(64, 64)
      };

      var image2 = {
        url: base_url + '/images/contact/marker-hover.png',
        size: new google.maps.Size(64, 64),
        anchor: new google.maps.Point(22, 86),
        scaledSize: new google.maps.Size(64, 64)
      };

      var shape = {
          coords: [1, 1, 1, 60, 44, 60, 44 , 1],
          type: 'poly'
      };

          var marker = new google.maps.Marker({
        position: markerLatLng,
        map: map,
        animation: google.maps.Animation.DROP,
        title:"Hello!",
        icon:image1,
        shape:shape
      });



      google.maps.event.addListener(marker, 'mouseover', function() {
          marker.setIcon(image2);
      });
      google.maps.event.addListener(marker, 'mouseout', function() {
          marker.setIcon(image1);
      });

      var x3url = 'https://www.google.com/maps/place/16+W+22nd+St,+New+York,+NY+10010/@40.7412079,-73.9913107,17z/data=!3m1!4b1!4m2!3m1!1s0x89c259a386c20bd5:0x6da26b75635d4e84';

      google.maps.event.addListener(marker, 'click', function() {
          window.open(x3url);
      });

      var getCen = map.getCenter();

      google.maps.event.addDomListener(window, 'resize', function() {
        map.setCenter(getCen);
      });

}); /* end of as page load scripts */