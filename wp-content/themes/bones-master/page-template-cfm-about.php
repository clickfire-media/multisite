<?php 
/*

Template Name: CFM About

*/

get_header(); ?>

		<div id="page-container">
        	 
        	<div id="page-content">

        		<div id="about-page" class="page-content-inner">
				
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="page-header">
							<div class="page-header-inner">
								<?php
									if(get_field('about_headline')) {
										echo '<h1>'.get_field('about_headline').'</h1>';
									}
									echo '<div class="page-header-description">';
										the_content(); 
									echo '</div>';
								?>
							</div>
						</div>

						<?php
							$services = get_field('services');

							if($services) {
								echo '<div class="section-header">
										<h2>Our Services</h2>
									</div>';

								echo '<div class="services">';
								foreach ($services as $key => $service) {
									echo '<div class="services-section">';
										// helper($service);
										echo $service['service'];
									echo '</div>';
								}
								echo '</div>';
							}
						?>
					
					<?php endwhile; endif; ?>

				</div>

			</div>

		</div>

<?php get_footer(); ?>