<?php // get_header(); ?>

<?php 

	$title = get_the_title();
	$p_projects = get_field('projects');
	$projects = array();
	foreach ($p_projects as $key => $project) {
		$id = $project->ID;
		$my_project = array();
		$my_project['title'] = $project->post_title;
		$my_project['sub_title'] = get_field('sub_title', $id);
		$my_project['description'] = get_field('description', $id);
		$my_project['link'] = get_field('link', $id);
		$my_project['additional_links'] = get_field('additional_links', $id);
		$my_project['thumbnail'] = get_field('thumbnail', $id);
		# code...

		array_push($projects, $my_project);
	}
	// helper($projects);

	$e_body = printHeader($title);

	foreach ($projects as $key => $project) {
		$e_body .= buildBlock($project);	
	}

	$e_body .= printFooter();

	// $e_body = file_get_contents(get_bloginfo('template_url').'/php/resp-email.html');
	
	// email
	$to = get_field('to');
	$subject = get_field('subject');
	$headers = array('Content-Type: text/html; charset=UTF-8');

	echo $e_body;

    

    wp_mail( $to, $subject, $e_body, $headers );
?>

<?php // get_footer(); ?>