<?php 

/*

Template Name: Click3X-intro

*/

get_header(); 

?>

		<div id="page-container">

			<div id="page-content">
        	 
	        	<div id="work-page" class="page-content-inner intro">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php 
							$page_links = get_field('page_links');
							$featured_projects = get_field('featured_projects');
						?>

						<div class="page-header">
							<div class="page-header-inner logo-tri-container">
								<ul class="logo-tri clearfix">
									<?php foreach ($page_links as $key => $page_link) : ?>
										<li class="<?php echo $page_link['link']; ?>">
						                	<div class="inner">
											<a href="<?php echo $page_link['link']; ?>">
												<div class="copy-container">
													<img src="<?php echo $page_link['image']['url']; ?>">
													<p><?php echo $page_link['caption']; ?></p>
												</div>
											</a>
											</div>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
							
						<div class="cfm-project-gallery">
							<ul>
								<?php foreach ($featured_projects as $key => $project) : ?>
									<?php 
										$terms = get_the_terms($project->ID, 'agency');
										if($terms) {
											$term = $terms[0]->name;
										}
										$poster = get_field('poster', $project->ID);

										if(get_field('project_link', $project->ID)) {
											$link = get_field('project_link', $project->ID);
										} else {
											$link = $project->guid;	
										}
									?>
									<li class="work-item">
										<div class="project-inner hoverfriend">
											<a target="_blank" href="<?php echo $link; ?>" class="cfm-project bg_image_view ready" style="background-image: url(<?php echo $poster['url']; ?>);">
												<div class="gray-filter"></div>
												<div class="red-gradient"></div>
											</a>
											<div class="project-label"><div class="project-label-inner">
												<h3 style="color:white"><?php echo get_field('client', $project->ID); ?></h3>
												<h2><?php echo $project->post_name; ?></h2>
												<div class="label-line"></div>
												<?php if(isset($term)) { echo '<h3>'.$term.'<?h3>'; } ?>
											</div></div>
										</div>
									</li>

								<?php endforeach; ?>
							</ul>
						</div>

					<?php endwhile; endif; ?>

					
				</div>

			</div>
			
		</div>
<?php get_footer(); ?>