<?php // get_header(); ?>

<?php 

	$title = get_the_title();
	$p_projects = get_field('projects');
	$projects = array();
	foreach ($p_projects as $key => $project) {
		$id = $project->ID;
		$my_project = array();
		$my_project['title'] = $project->post_title;
		$my_project['sub_title'] = get_field('sub_title', $id);
		$my_project['description'] = get_field('description', $id);
		$my_project['link'] = get_field('link', $id);
		$my_project['additional_links'] = get_field('additional_links', $id);
		$my_project['thumbnail'] = get_field('thumbnail', $id);
		# code...

		array_push($projects, $my_project);
	}
	// helper($projects);

	$e_body = printHeader_cfm($title);

	foreach ($projects as $key => $project) {
		$e_body .= buildBlock_cfm($project);	
	}

	// $form = '<form id="myForm" action="'.$_SERVER['PHP_SELF'].'" method="post"><label for="send-email">Send Email</label>
 //    <input type="radio" name="send-email" id="send-email" tabindex="3" value="__return_true();"><input type="submit">submit</input></form>';

	// $e_body .= $form;
	
	$e_body .= printFooter_cfm();
	
	// email
	$to = get_field('to');
	$subject = get_field('subject');
	$headers = array('Content-Type: text/html; charset=UTF-8');

	echo $e_body;

    // wp_mail( $to, $subject, $e_body, $headers );
?>

<?php // get_footer(); ?>