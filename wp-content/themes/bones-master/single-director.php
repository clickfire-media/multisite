<?php get_header(); ?>

			<div id="page-container">
        	 
        		<div id="page-content">
					<div id="director-page" class="page-content-inner">
						<div id="director-details-container">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<div class="title-label clearfix">
									<?php
										$prev_post = get_previous_post();
										if($prev_post) {
										   $prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));
										   echo '<nav class="nav-fillpath director-prev"><a class="prev" href="'.get_permalink($prev_post->ID).'"><span class="icon-wrap"></span><h3>'.$prev_title.'</h3></a></nav>';
										}

										$next_post = get_next_post();
										if($next_post) {
										   $next_title = strip_tags(str_replace('"', '', $next_post->post_title));
										   echo '<nav class="nav-fillpath director-next"><a class="next" href="'.get_permalink($next_post->ID).'"><span class="icon-wrap"></span><h3>'.$next_title.'</h3></a></nav>';
										}
									?>								
								</div>


							<?php 
								echo '<div class="page-header"><div class="page-header-inner">';
									echo '<h3 class="director-heading">DIRECTOR</h3>';
									echo '<h1>'.get_the_title().'</h1>';
										if(get_field('bio')) { 
											echo '<p class="hidden-bio">'.get_field('bio').'</p>';
											echo '<a class="view-more"><span>MORE</span><div class="more-underline"></div></a>';
										}
								echo '</div></div>';
							?>

								<?php
									get_template_part( 'post-formats/format-director', get_post_format() );
								?>

							<?php endwhile; ?>

							<?php endif; ?>

						</div>
					</div>
				</div>

			</div>

<?php get_footer(); ?>