<?php // get_header(); ?>

<?php 

	$p_projects = get_field('projects');
	$projects = array();
	foreach ($p_projects as $key => $project) {
		$id = $project->ID;
		$my_project = array();
		$my_project['title'] = $project->post_title;
		$my_project['sub_title'] = get_field('sub_title', $id);
		$my_project['description'] = get_field('description', $id);
		$my_project['link'] = get_field('link', $id);
		$my_project['thumbnail'] = get_field('thumbnail', $id);
		# code...

		array_push($projects, $my_project);
	}
	// helper($projects);

	$head = get_bloginfo('template_directory').'/html/email-head.html';
	$foot = get_bloginfo('template_directory').'/html/email-foot.html';

	// $e_body = '';
	// $e_body .= file_get_contents($head);
	$e_body = file_get_contents($head);

	foreach ($projects as $key => $project) {
		$e_body .= buildBlock($project);	
	}
	// $e_body .= buildBlock($projects[0]);	
	$e_body .= file_get_contents($foot);
	
	echo $e_body;


	// email
	$to = 'charles@click3x.com';
    $subject = 'Click 3x at it again!';

    $headers = array('Content-Type: text/html; charset=UTF-8');
     

    wp_mail( $to, $subject, $e_body, $headers );
?>

<?php // get_footer(); ?>