<?php 

/*

Template Name: Click3X-contact

*/

get_header(); 

?>

		<div id="page-container">

			<div id="page-content">
        	 
	        	<div id="contact-page" class="page-content-inner">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<?php 
							$logo = get_field('logo');
							$contact_headline = get_field('contact_headline');
							$address = get_field('address');
							$contacts = get_field('contacts');
						?>
						
						<?php if($contact_headline) : ?>					
					    	<div class="title-label"><h3><?php echo $contact_headline; ?></h3></div>
						<?php endif; ?>

					    <?php if($address) : ?>
						    <div class="address">
						        <?php echo $address; ?>
						    </div>
						<?php endif; ?>

						<?php if($contacts) : ?>
						    <div class="inquiry-container">
						    	<?php 
						    		foreach ($contacts as $key => $contact) {
						    			echo '<div class="inquiry-sales">';
						    				echo '<div class="inquiry-inner">';
						    					echo $contact['contact'];
						    				echo '</div>';
						    			echo '</div>';
						    		}
						    	?>
						    </div>
						<?php endif; ?>

					<?php endwhile; endif; ?>

					<div class="cfm-google-map">
						<div id="map"></div>
					</div>
					
				</div>

			</div>
			
		</div>
<?php get_footer(); ?>