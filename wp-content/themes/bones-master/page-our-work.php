<?php get_header(); ?>

<?php 
	$args = array(
		'post_type'   		=> 'project',
		'posts_per_page' 	=> -1,
	);

	$query = new WP_Query( $args );	
?>

		<div id="page-container">
        	 
        	<div id="page-content">

        		<div id="projects-page" class="page-content-inner">
					<div class="cfm-project-gallery">
						<ul>

							<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

							<?php 
								$gallery_thumbnail = get_field('gallery_thumbnail');
								$gallery_thumbnail = $gallery_thumbnail['url'];
								$link = $post->guid;
								$title = get_the_title();
								if(!$gallery_thumbnail) { $gallery_thumbnail = ''; }
							?>
							
							<li data-id="33" data-image="<?php echo $gallery_thumbnail; ?>">
								<div class="project-inner">
									<a class="cfm-project bg_image_view ready" href="<?php echo $link; ?>" data-navigate-to="" style="background-image: url(<?php echo $gallery_thumbnail; ?>);">
										<div class="project-label"><div class="project-label-inner"><h2><?php echo $title; ?></h2></div></div>
									</a>
								</div>
							</li>
							
							<?php endwhile; endif; ?>

						</ul>
					</div>
				</div>

			</div>

		</div>

<?php get_footer(); ?>