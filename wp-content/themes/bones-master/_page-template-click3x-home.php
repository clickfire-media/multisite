<?php 
/*

Template Name: Click3x-Home

*/

get_header(); 

// // GET PROJECTS FOR THE FEATRUED PROJECTS SECTION OF HOME PAGE
// $projects = array();

// $args = array(
//     'post_type' => 'project',
//     'posts_per_page' => -1,
//     'tax_query' => array(                     //(array) - use taxonomy parameters (available with Version 3.1).
//         'relation' => 'AND',                      //(string) - Possible values are 'AND' or 'OR' and is the equivalent of ruuning a JOIN for each taxonomy
//         array(
//             'taxonomy' => 'project_category',                //(string) - Taxonomy.
//             'field' => 'slug',                    //(string) - Select taxonomy term by ('id' or 'slug')
//             'terms' => array( 'featured' ),    //(int/string/array) - Taxonomy term(s).
//             'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
//             'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
//         )
//     )
// );

// $the_query = new WP_Query( $args );

// if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();

// 	$my_project = array();
//     $my_project["title"] = get_the_title();
//     $my_project["id"] = get_the_ID();
//     if(get_field('project_link')) {
//     	$my_project["permalink"] = get_field('project_link');
//     } else {
//     	$my_project["permalink"] = get_the_permalink();
//     }
//     if(get_field('client')) {
//     	$my_project["client"] = get_field('client');
//     }
//     if(get_field('poster')) {
//     	$my_project["poster"] = get_field('poster');
//     }
//     if(get_field('video')) {
//     	$my_project["video"] = get_field('video');
//     }
//     if(get_field('description')) {
//     	$my_project["description"] = get_field('description');
//     }

//     array_push($projects, $my_project);

// endwhile; 
// endif;

// wp_reset_postdata();


// helper($projects);


?>

		<div id="page-container">
        	 
        	<div id="page-content">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php 
						$page_links = get_field('page_links');
						$p_projects = array();

						foreach ($page_links as $key => $page_link) {
							$m_project = array();
							$m_project['id'] = $page_link['page_link']->ID;
							$m_project['title'] = $page_link['page_link']->post_title;
							$m_project['permalink'] = $page_link['page_link']->guid;

							if(get_field('featured_post', $m_project['id'])) {
								$feat_post = get_field('featured_post', $m_project['id']);
								$f_id = $feat_post->ID;

								$m_project['project_title'] = $feat_post->post_title;
								if(get_field('client', $f_id)) {
							    	$m_project["client"] = get_field('client', $f_id);
							    } 

							    if(has_post_thumbnail($page_link['page_link']->ID)) {
							    	$m_project["poster"]["url"] =  wp_get_attachment_url( get_post_thumbnail_id($page_link['page_link']->ID) );
							    } else if(get_field('poster', $f_id)) {
							    	$m_project["poster"] = get_field('poster', $f_id);
							    }

							    if(get_field('video', $f_id)) {
							    	$m_project["video"] = get_field('video', $f_id);
							    }
							    if(get_field('description', $f_id)) {
							    	$m_project["description"] = get_field('description', $f_id);
							    }
							} 
							// helper($m_project);
							array_push($p_projects, $m_project);
						}
					?>
					
					<div id="home-page" class="page-content-inner">

						<div class="featured-carousel-container">
							<div class="home-swiper-container swiper-container-horizontal">
							    <!-- Additional required wrapper -->
							    <div class="swiper-wrapper">
							    	<!-- Slides -->
							    	<?php foreach ($p_projects as $key => $project) : ?>
							    		<!-- <div class="swiper-slide" data-swiper-slide-index="<?php echo $key; ?>"> -->
							    		<div class="swiper-slide">
								      		<a data-id="<?php echo $project['id']; ?>" class="featured-item" data-filename="<?php echo $project['video']['title']; ?>"><div class="featured-poster" style="background-image: url(<?php echo $project['poster']['url']; ?>);"><div class="gray-filter"></div></div></a>
								        	<div class="video-credits">
								        		<h2><?php echo $project['client']; ?></h2>
								        		<a href="<?php echo $project['permalink']; ?>" data-filename="c3xvr" class="featured-item"><h1><?php echo $project['title']; ?></h1></a>
									        	<div class="label-line"></div>
									        </div>
									   	</div>
								    			
							    	<?php endforeach; ?>
							    </div>
							    <!-- fancy animatiion *_* -->
							    <div id="animation-over-slides">             										
									<div id="shape-container" style="display: none; left: 100%;">
					    				<span class="Shape_mid"></span>
					    				<span class="Shape_base"></span>
					    			</div>	
					                <div id="shape-containerA" style="left: 100%; display: none;">
					    				<span class="ShapeA_mid"></span>
					    				<span class="ShapeA_base"></span>
					    			</div>	
										<div id="shape-container2" style="display: none; left: -100%;">
					    				<span class="Shape2_mid"></span>
					    				<span class="Shape2_base"></span>
					    			</div>	
					                <div id="shape-container2A" style="left: -100%; display: none;">
					    				<span class="Shape2A_mid"></span>
					    				<span class="Shape2A_base"></span>
					    			</div>	
								</div>
							    
							    <div class="swiper-pagination swiper-pagination-clickable"><span class="swiper-pagination-bullet"><span class="swiper-bullet-inner"></span></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active"><span class="swiper-bullet-inner"></span></span><span class="swiper-pagination-bullet"><span class="swiper-bullet-inner"></span></span></div>

					            <div id="company-container" class="company-container clearfix">
					            	<?php foreach ($p_projects as $key => $project) : ?>
					            		<!-- account for the active index -->
					            		<?php
					            			if($key == 0) {
					            				echo '<div class="active-index"><a href="'.$project['permalink'].'">'.$project['title'].'</a></div>';
					            			} else {
					            				echo '<div class=""><a href="'.$project['permalink'].'">'.$project['title'].'</a></div>';
					            			}
					            		?>
					            	<?php endforeach; ?>
					            </div>

							</div>
						</div>	

						<!-- OVERLAY -->
					    <div class="video-overlay overlay-scale" id="featured-overlay-container">
					        <div class="video-overlay-inner">
					            <div class="overlay-close"></div>
					            <div id="featured-overlay-slider">
					                <div class="featuredOverlay-swiper-container">
					                    <!-- Additional required wrapper -->
					                    <div class="swiper-wrapper">
					                        
					                    	<?php foreach ($p_projects as $key => $project) : ?>
						                        <!-- Slides -->
						                        <div class="swiper-slide" id="featuredOverlay-slide-<?php echo $project['id']; ?>">
						                            <div class="overlay-credits">
						                                <h3><?php echo $project['client']; ?></h3>
						                                <h1><?php echo $project['project_title']; ?></h1>
						                                <div class="label-line"></div>
						                                <h2></h2>
						                            </div>
						                            <!-- video -->
						                            <div id="featured-video-<?php echo $project['id']; ?>" class="cfm-videoplayer" data-video-name="<?php echo $project['video']['url']; ?>" data-poster="<?php echo $project['poster']['url']; ?>">
						                                <div class="cfm-videoplayer-inner">
						                                    <video class="cfm-videoplayer-mobile" width="960" height="540" controls=""></video>
						                                    <div class="cfm-videoplayer-poster">
						                                        <div class="cfm-videoplayer-poster-header">
						                                            <div class="cfm-videoplayer-poster-header-inner">
						                                                <div class="cfm-videoplayer-playbutton"><span class="arrow"></span></div>
						                                            </div>
						                                        </div>
						                                    </div>
						                                    <video class="cfm-videoplayer-desktop" width="960" height="540"></video>
						                                    <div class="cfm-video-controls">
						                                        <ul>
						                                            <li class="cfm-video-play-pause-btn cfm-video-btn"></li>
						                                            <li class="cfm-video-fullscreen-btn cfm-video-btn"></li>
						                                            <li class="cfm-video-mute-btn cfm-video-btn"></li>
						                                        </ul>
						                                        <div class="cfm-video-progress-container"><a class="cfm-video-seek-bar"></a><input class="cfm-video-seek-bar-input" type="range" value="0"></div>
						                                    </div>
						                                </div>
						                            </div>
						                            <!-- end of video -->                                 
						                        </div>
						                        <!-- end slide -->
						                    <?php endforeach; ?>

					                    </div>
					                    <div class="swiper-button-prev overlay-nav" id="arrow-left"><a class="prev"><span class="icon-wrap"></span></a></div>
					                    <div class="swiper-button-next overlay-nav" id="arrow-right"><a class="next"><span class="icon-wrap"></span></a></div>
					                </div>
					            </div>
					        </div>
					    </div>
					    <!-- end of overlay -->	


					</div>
				
				<?php endwhile; endif; ?>

			</div>

		</div>

<?php get_footer(); ?>