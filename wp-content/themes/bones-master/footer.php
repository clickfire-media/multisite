			<?php $b_id = get_current_blog_id(); ?>
            <div id="footer-container" class="clearfix">

                <?php 
                    // FOOTER LINKS
                    if(get_field('footer_logo', 'option')) { 
                        $footer_logo = get_field('footer_logo', 'option');
                        // echo '<div class="cfm-logo-footer" style="width:'.$footer_logo['width'].'px; height:'.$footer_logo['height'].'px; background-image:url('.$footer_logo['url'].');"></div>';
                        // echo '<h1> THis is $b_id!'.$b_id.'</h1>';
                        if($b_id == 3) {
                            echo '<div class="cfm-logo-footer" style="background-image:url('.$footer_logo['url'].');"></div>';                            
                        } else {
                            echo '<div class="cfm-logo-footer" style="width:'.$footer_logo['width'].'px; height:'.$footer_logo['height'].'px; background-image:url('.$footer_logo['url'].');"></div>';
                        }
                    }
                    if(get_field('social_links', 'option')) { 
                        $social_links = get_field('social_links', 'option');

                        if($b_id == 3) {
                            echo '<div class="social-navigation">';
                                echo '<ul>';
                                foreach ($social_links as $key => $link) {
                                    echo '<li id="p'.($key+1).'" class="'.$link['social_media_title'].'-btn"><div class="project-inner"><a href="'.$link['social_media_link'].'" target="_blank"><div class="project-label"><div class="project-label-inner"></div></div></a></div></li>';
                                }
                                echo '</ul>';
                            echo '</div>';
                        } else {
                            echo '<div class="social-navigation">';
                                echo '<ul class="social-links cf">';
                                foreach ($social_links as $key => $link) {
                                    echo '<li id="p'.($key+1).'" class="'.$link['social_media_title'].'-btn"><div class="project-inner"><a href="'.$link['social_media_link'].'" target="_blank"><div class="project-label"><div class="project-label-inner"></div></div></a></div></li>';
                                }
                                echo '</ul>';
                            echo '</div>';
                        }
                    }

                    echo '<p>';
                        if(get_field('phone', 'option')) {
                            echo '<a href="tel:'.get_field('phone', 'option').'" class="phone">'.get_field('phone', 'option').'</a>';
                            echo '<br>';
                        }
                        $addresses = array('street_address', 'city_state', 'zip');
                        foreach ($addresses as $key => $address) {
                            if(get_field($address, 'option')) {
                                echo '<span class="address-part '.$address.'">'.get_field($address, 'option').'</span>';
                            }
                        }

                        if(get_field('email', 'option')) {
                            echo '<br>';
                            echo '<a href="mailto:'.get_field('email', 'option').'" class="email">'.get_field('email', 'option').'</a>';
                        }
                        if($b_id == 3) {
                            echo '<br>';
                            echo '<a href="'.get_the_permalink(14).'" class="email" style="text-decoration:none;">Contact Us</a>';   
                        }
                    echo '</p>';
                ?>
			</div>
            <!-- <div id="mobile-menu-outer" class="cfm-navigation mobile-menu"> -->
            <div id="mobile-menu" class="cfm-navigation mobile-menu">
                <div id="hamburger-open">
                    <div class="hamburger-inner">
                        <a>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                </div>
                <div class="mobile-menu-inner">
                    <div class="logo-white-may">
                        <a href="http://click3x.com/intro"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 398 80" enable-background="new 0 0 398 80" xml:space="preserve">
                            <g>
                                <path fill="#ffffff" d="M66.4,64.8c-6.8,9.4-16.2,13.2-26.8,13.2C17.5,77.9,2.2,62,2.2,40S17.5,2.1,39.6,2.1c9.8,0,18.6,3.3,24.9,11.3L59,18
                                    c-4.5-6.7-12.1-9.8-19.4-9.8C20.7,8.2,9,22,9,40s11.7,31.8,30.6,31.8c7.2,0,16.2-3.5,21.3-11.2L66.4,64.8z"></path>
                                <path fill="#ffffff" d="M78.7,3.9h6.7V70h33.7v6.1H78.7V3.9z"></path>
                                <path fill="#ffffff" d="M129.7,3.9h6.7v72.2h-6.7V3.9z"></path>
                                <path fill="#ffffff" d="M215.7,64.8c-6.8,9.4-16.2,13.2-26.8,13.2c-22,0-37.3-15.9-37.3-37.9s15.3-37.9,37.3-37.9c9.8,0,18.6,3.3,24.9,11.3
                                    l-5.5,4.6c-4.5-6.7-12.1-9.8-19.4-9.8C170,8.2,158.3,22,158.3,40s11.7,31.8,30.6,31.8c7.2,0,16.2-3.5,21.3-11.2L215.7,64.8z"></path>
                                <path fill="#ffffff" d="M228.1,3.9h6.7v31.8h0.6l35-31.8h9.3l-36.5,33l38.6,39.2H272l-36.6-37.3h-0.6v37.3h-6.7V3.9z"></path>
                                <path fill="#ffffff" d="M291.3,61c2.7,6.9,7,10.8,15.2,10.8c10.7,0,16.1-7,16.1-15.7c0-10.7-9.9-14.9-19.1-14.9h-2.7v-6.1h3.1
                                    c7.8,0,16.1-3.8,16.1-13.7c0-7.7-5.3-13.3-13.4-13.3c-5.9,0-10.9,3.3-13.7,8.5l-5.8-3.3c4-7.4,11.2-10.7,19.5-10.7
                                    c11,0,20.1,6.1,20.1,18.6c0,7.9-4.5,14.5-12.9,16.7v0.2c9.2,1.6,15.4,9.3,15.4,18.5c0,11.4-7.4,20.7-22.3,20.7
                                    c-10.8,0-18.6-4.4-22.3-14.2L291.3,61z"></path>
                                <path fill="#ffffff" d="M360.8,38.2L336.1,3.9h8l20.8,29.4l20.5-29.4h8.1l-24.5,34.3l26.8,37.9h-8.4l-22.8-32.6l-22.8,32.6h-8.1L360.8,38.2z"></path>
                            </g>
                        </svg></a>
                    </div>
                    <?php wp_nav_menu(array(
                         'container' => false,                           // remove nav container
                         'menu_id'=>'mobile-menu',
                         'container_class' => 'menu cf',                 // class of container (should you choose to use it)
                         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
                         'menu_class' => 'cf',               // adding custom nav class
                         'theme_location' => 'main-nav',                 // where it's located in the theme
                         'before' => '',                                 // before the menu
                           'after' => '',                                  // after the menu
                           'link_before' => '',                            // before each link
                           'link_after' => '',                             // after each link
                           'depth' => 0,                                   // limit the depth of the nav
                         'link_before'=>'<h1>',
                         'link_after'=>'</h1>'
                    )); ?>
                </div>
            </div>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

        <?php
            // CONDITIONALLY LOAD OUTLINE SCRIPT
            $server = $_SERVER['REMOTE_ADDR'];
            // IF SERVER IS LOCAL, ADD OUTLINE BUTTON
            if($server == '127.0.0.1') {
                echo "<script>
                    jQuery('head').append('<style>#outline {position:fixed;z-index:1000;bottom:50px;right:50px; width:50px; height:50px; border:2px solid white; cursor:pointer;} .outlines {outline:1px solid rgba(255, 0, 0, 0.3);}</style>');
                    jQuery('body').append('<input id=\"outline\" type=\"button\">');

                    jQuery('#outline').click(function() {
                        jQuery('*').toggleClass('outlines');
                   });
                </script>";
            }
        ?>

	</body>

</html> <!-- end of site. what a ride! -->