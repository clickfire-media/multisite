<?php 

    // helper($post);

    global $post;
    $slug = $post->post_name;

    // PART 1 - BRAND COPY + IMAGES
    $title = get_the_title();

    $projects = get_field('projects');
    $d_id = $post->ID;
?>


<?php if($projects) : ?>
    <div class="director-videos-list">
        <ul>
            <?php foreach ($projects as $key => $project) : ?>
                <?php 
                    // helper($project);
                    $title = $project['title'];
                    if($project['poster']) {
                        $poster = $project['poster'];
                        $url = $poster['url'];
                    } else {
                        $url = '';
                    }
                    if($project['video']) {
                        $video = $project['video'];
                        // helper($video);
                        $vid_filename = $video['title'];
                        $vid_url = $video['url'];
                    } else {
                        $vid_url = '';
                    }

                ?>
                <li class="director-item hoverfriend" data-id="<?php echo $key; ?>" data-filename="<?php echo $vid_url; ?>">
                    <a style="background-image: url(<?php echo $url;?>);"><div class="red-gradient"></div></a>
                    <div class="video-label">
                        <?php if(isset($project['client'])) {
                            echo '<h3>'.$project['client'].'</h3>';
                         } ?>
                        <h2><?php echo $title; ?></h2>
                        <div class="label-line"></div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <!-- OVERLAY -->
    <div class="video-overlay overlay-scale" id="director-overlay-container">
        <div class="video-overlay-inner">
            <div class="overlay-close"></div>

            <div id="director-overlay-slider">
                <div class="directorOverlay-swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <?php foreach ($projects as $key => $project) : ?>
                            <?php // helper($project); ?>
                            <div class="swiper-slide" id="directorOverlay-slide-<?php echo $key; ?>">
                                <div class="overlay-credits">
                                    <h3><?php echo $project['client'];?></h3>
                                    <h1><?php echo $project['title'];?></h1>
                                    <div class="label-line"></div>
                                    <h2><?php echo get_the_title();?></h2>
                                </div>
                                <!-- video -->
                                <div id="director-video-<?php echo $key; ?>" class="cfm-videoplayer" data-video-name="<?php echo $project['video']['url']; ?>" data-poster="<?php echo $project['poster']['url']; ?>">
                                    <div class="cfm-videoplayer-inner">
                                        <video class="cfm-videoplayer-mobile" width="960" height="540" controls></video>
                                        <div class="cfm-videoplayer-poster">
                                            <div class="cfm-videoplayer-poster-header">
                                                <div class="cfm-videoplayer-poster-header-inner">
                                                    <div class="cfm-videoplayer-playbutton"><span class="arrow"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <video class="cfm-videoplayer-desktop" width="960" height="540"></video>
                                        <div class="cfm-video-controls">
                                            <ul>
                                                <li class="cfm-video-play-pause-btn cfm-video-btn"></li>
                                                <li class="cfm-video-fullscreen-btn cfm-video-btn"></li>
                                                <li class="cfm-video-mute-btn cfm-video-btn"></li>
                                            </ul><div class="cfm-video-progress-container"><a class="cfm-video-seek-bar"></a><input class="cfm-video-seek-bar-input" type="range" value="0"></div>
                                        </div>
                                    </div>
                                </div><!-- end of video -->                                 
                            </div><!-- end of slide -->


                        <?php endforeach; ?>
                    </div>

                    <div class="swiper-button-prev overlay-nav" id="arrow-left"><a class="prev"><span class="icon-wrap"></span></a></div>
                    <div class="swiper-button-next overlay-nav" id="arrow-right"><a class="next"><span class="icon-wrap"></span></a></div>

                </div>
            </div>
        </div>
    </div><!-- end of overlay -->

<?php endif; ?>