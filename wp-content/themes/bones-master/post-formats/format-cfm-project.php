<div class="project-detail">
    <?php 

        // helper($post);

        global $post;
        $slug = $post->post_name;

        // PART 1 - BRAND COPY + IMAGES
        $title = get_the_title();
        $banner_image = get_field('banner_image');
        $headline = get_field('headline');
        $sub_headline = get_field('sub_headline');
        $bio = get_field('bio');
        $site_url = get_field('site_url');
        if(get_field('site_url_text')) {
            $site_url_text = get_field('site_url_text');    
        } else {
            $site_url_text = 'VISIT SITE';
        }
        $brand_image = get_field('brand_image');
        $gallery_thumbnail = get_field('gallery_thumbnail');

        // CLIENT, CREDITS, PLATFORMS
        $client = get_field('client');
        $credits = get_field('credits');
        $platforms = get_field('platforms');

        // PART 3 CAROUSEL AND VIDEO
        $carousels = get_field('carousels');
        $banner_videos = get_field('banner_videos');
    ?>

    <!-- PROJECT HEADING -->
    <div class="project-heading">
        <div class="project-heading-inner">
        <?php 
            if($headline) { echo '<h2>'.$headline.'</h2>'; } 
            if($sub_headline) { echo '<h1>'.$sub_headline.'</h1>'; } 
            if($banner_videos) { echo '<div class="video-anchor-link"><a href="#'.$slug.'-video"><span class="arrow"></span><span class="label">Watch Case Study</span></a></div>'; }
        ?>
        </div>
    </div>


    <!-- FEATURED MODULE -->
    <div class="featured-module">
        <div class="featured-module-inner project-module-inner">
            <?php 
                if($banner_image) {
                    echo '<div class="project-module-image bg_image_view" data-image="'.$banner_image['url'].'"></div>';
                } 
            ?>
        </div>
    </div>


    <!-- PROJECT ABOUT -->
    <div class="project-about">
        <div class="project-about-inner">
            <?php 
                if($brand_image) {
                    echo '<div class="project-module-image project-logo bg_image_view" data-image="'.$brand_image['url'].'"></div>';
                }
            ?>
            
            <div class="project-description">
                <div class="project-description-inner">
                    <?php 
                        if($bio) { echo '<p>'.$bio.'</p>'; }

                        if( $client || $credits || $platforms ) {
                            echo '<p class="credits">';
                            if($client) { echo '<span class="description-label">Client:</span>'.$client.'<br>'; }
                            if($credits) { echo '<span class="description-label">Credits:</span>'.$credits.'<br>'; }
                            if($platforms) { echo '<span class="description-label">Platforms:</span>'.$platforms; }
                            echo '</p>';
                        }

                        if($site_url) {
                            echo '<div class="project-links-menu">
                                <div class="project-links-menu-inner">
                                    <ul>
                                        <li><a target="_blank" href="'.$site_url.'"><h4>'.$site_url_text.'</h4></a></li>
                                    </ul>
                                </div>
                            </div>';
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>


    <!-- CAROUSELS -->
    <?php if($carousels) : ?>
        <div class="the-answer-container project-component-container">
        <?php 
            foreach ($carousels as $key => $carousel) {
                $description = $carousel['description'];
                $car_imgs = $carousel['carousel'];
                if($car_imgs) {
                    echo '<div class="the-answer-carousel owl-carousel owl-theme">';
                        foreach ($car_imgs as $key => $img) {
                            $url = $img['image']['url'];
                            echo '<div class="item"><img src="'.$url.'"></div>';
                        }
                    echo '</div>';
                }

                if($description) {
                    echo '<div class="after-text-container"><p>'.$description.'</p></div>';
                }
            }
        ?>
        </div>
    <?php endif; ?>


    <!-- BANNER VIDEO -->
    <?php if($banner_videos) : ?>
        <div class="project-module-container">
            <?php foreach ($banner_videos as $key => $banner_video) : ?>
                <div class="project-module banner-video">
                    <div class="project-module-inner">
                        <?php if($key == 0) :?>
                            <div id="<?php echo $slug; ?>-video" class="project-module-description"></div>
                        <?php endif; ?>
                        <div class="cfm-videoplayer project-module-video ready" data-video="<?php echo $banner_video['video']['url'];?>" data-poster="<?php echo $banner_video['poster']['url'];?>">
                            <div class="cfm-videoplayer-inner">
                                <div class="cfm-videoplayer-poster bg_image_view ready" style="background-image: url(<?php echo $banner_video['poster']['url'];?>);">
                                    <div class="cfm-videoplayer-poster-header">
                                        <div class="cfm-videoplayer-poster-header-inner">
                                            <div class="cfm-videoplayer-playbutton ">
                                                <?php 
                                                    if($key == 0) {
                                                        echo '<span class="label">Watch Case Study</span>';
                                                    } else {
                                                        echo '<span class="label">About '.$title.'</span>';
                                                    }
                                                ?>
                                                <span class="arrow"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <video class="cfm-videoplayer-desktop" width="960" height="540" type="<?php echo $banner_video['video']['mime_type'];?>" src="<?php echo $banner_video['video']['url'];?>" style="height: 100%; width: auto;"></video>
                                <div class="cfm-video-controls">
                                    <ul>
                                        <li class="cfm-video-play-pause-btn cfm-video-btn"></li>
                                        <li class="cfm-video-fullscreen-btn cfm-video-btn"></li>
                                        <li class="cfm-video-mute-btn cfm-video-btn"></li>
                                    </ul>
                                    <div class="cfm-video-progress-container" style="background-size: 13.4568% 40px;">
                                        <a class="cfm-video-seek-bar"></a>
                                        <input class="cfm-video-seek-bar-input" type="range" value="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    <?php endif; ?>

</div>