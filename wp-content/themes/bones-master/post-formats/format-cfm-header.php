<div class="page-header-navigation">
    <div class="page-header-inner">
        <div class="cfm-project-naviation cfm-navigation">
            <ul>
                <li><a href="<?php echo get_permalink(39); ?>" data-navigate-to="home"><span class="line-arrow-left line-arrow"></span><h4>Home</h4></a></li>
                <?php
                    $prev_post = get_previous_post();
                    if($prev_post) {
                       $prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));
                       echo "\t" . '<li class="previous-button"><a rel="prev" href="' . get_permalink($prev_post->ID) . '" title="' . $prev_title. '" class=" "><span class="line-arrow-left line-arrow"></span></a></li>' . "\n";
                    }

                    $next_post = get_next_post();
                    if($next_post) {
                       $next_title = strip_tags(str_replace('"', '', $next_post->post_title));
                       echo "\t" . '<li class="next-button"><a rel="next" href="' . get_permalink($next_post->ID) . '" title="' . $next_title. '" class=" "><span class="line-arrow-right line-arrow"></span></a></li>' . "\n";
                    }
                ?>
            </ul>
        </div>
    </div>
</div><!--  /.page-header-navigation -->