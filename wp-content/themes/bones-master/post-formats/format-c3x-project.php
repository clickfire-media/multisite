<?php
    
    global $post;
    $slug = $post->post_name;

    // PART 1 - BRAND COPY + IMAGES
    $title = get_the_title();
    
    $client = get_field('client');
    $poster = get_field('poster');

    $video = get_field('video');
    $description = get_field('description');
    
    $site_url = get_the_permalink();
    if(get_field('project_link')) {
        $site_url = get_field('project_link');
    }

?>

<div class="single-project-container">
    <div class="overlay-credits">
        <h3><?php echo $client; ?></h3>
        <h1><?php echo $title; ?></h1>
        <div class="label-line"></div>
    </div>

    <!-- start video -->
    <div id="work-video-<?php echo $slug; ?>" class="cfm-videoplayer" data-video-name="<?php if($video) { echo $video['url']; } ?>" data-poster="<?php echo $poster['url']; ?>">
        <div class="cfm-videoplayer-inner">

            <video class="cfm-videoplayer-mobile" width="960" height="540" controls></video>
            <div class="cfm-videoplayer-poster">
                <div class="cfm-videoplayer-poster-inner">
                    <div class="cfm-videoplayer-playbutton"><span class="arrow"></span></div>
                </div>
            </div>
            <video class="cfm-videoplayer-desktop" width="960" height="540" loop="" autoplay></video>
            <div class="cfm-video-controls">
                <ul>
                    <li class="cfm-video-play-pause-btn cfm-video-btn"></li>
                    <li class="cfm-video-fullscreen-btn cfm-video-btn"></li>
                    <li class="cfm-video-mute-btn cfm-video-btn"></li>
                </ul><div class="cfm-video-progress-container"><a class="cfm-video-seek-bar"></a><input class="cfm-video-seek-bar-input" type="range" value="0"></div>
            </div>

        </div>
    </div><!-- end of video -->




    <!-- bottom description -->
    <div class="overlay-credits bottom-description">
        <div class="page-header"><div class="page-header-inner">
            <?php if($description) {
                echo '<p>'.$description.'</p>';
            }
            ?>
        </div></div>
    </div>

</div>