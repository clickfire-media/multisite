<?php 

    // $to = get_field('to');
    // $subject = get_field('subject');
    $projects = get_field('projects');



    // helper($projects);
    ob_start();
    $body_content = '';
    foreach ($projects as $key => $project) {
        $body_content .= '<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" style="width: 800px; max-width: 800px; background-color:#f1f1f1;">
            <tbody>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td class="content">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="content_row" align="center" style="width: 800px;">
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="one_half1" width="400">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="mobile_centered_table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <img editable="true" src="'.$project['thumbnail']['url'].'" width="100%" height="auto" border="0" style="display: block;" alt="blog image">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>

                                                                    <td class="nomobile" width="20"></td>

                                                                    <td class="one_half1" width="400" valign="top">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="mobile_centered_table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="padding" height="20px"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="dark_heading" data-editable="" align="center" style="color:#393939; font-weight: 700; font-size: 18px; font-family: \"Montserrat\", sans-serif; text-transform: uppercase; line-height: 18px; text-align: left;">
                                                                                        '.$project['title'].'
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td height="15"></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td class="red_heading" data-editable="" align="center" style="color:#b4122d; font-weight: 400; font-size: 13px; font-family: "Lato", sans-serif; text-transform: none; line-height: 13px; text-align: left; font-style: italic;">
                                                                                        '.$project['sub_title'].'
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td height="15"></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td class="dark_text" data-editable="" align="center" style="color:#686868; font-weight: 400; font-size: 13px; font-family: "Lato", sans-serif; text-transform: none; line-height: 25px; text-align: left;">
                                                                                        '.$project['description'].'
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td height="25"></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td class="button" data-editable="" height="40" width="132" bgcolor="#b4122d" style="font-family: "Montserrat", sans-serif; font-size: 12px; color: #ffffff; text-align: center; font-weight: 700; text-transform: uppercase; border-radius: 20px;">
                                                                                                        <a class="button1_text" href="'.$project['link'].'" style="text-decoration: none; color: #ffffff;">
                                                                        view project
                                                                    </a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="padding" height="20px"></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>

                                                    <td id="no-mobile-padding" class="no-mobile-padding padding" style="width: 20px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

        <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper_table" width="800px" style="max-width: 800px;">
            <tbody>
                <tr>
                    <td class="section_h" height="40"></td>
                </tr>
            </tbody>
        </table>';
    }
    ob_end_clean();

    // echo $body_content;


    $to = 'charliekuldip@gmail.com';
    $subject = 'The subject';
    // $file = get_bloginfo('template_directory').'/html/email.html';
    $head = get_bloginfo('template_directory').'/html/email-head.html';
    $file = get_bloginfo('template_directory').'/html/email-template-one.html';
    $foot = get_bloginfo('template_directory').'/html/email-foot.html';

    $html_file = file_get_contents($head);

    $html_file .= $body_content;
    $html_file .= file_get_contents($foot);

    // $body = file_get_contents($file);

    $headers = array('Content-Type: text/html; charset=UTF-8');
     
    
    wp_mail( $to, $subject, $html_file, $headers );

?>